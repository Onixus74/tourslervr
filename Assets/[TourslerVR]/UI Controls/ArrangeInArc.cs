﻿using System;
using UnityEngine;
using System.Collections;
using System.Linq;

[ExecuteInEditMode]
public class ArrangeInArc : MonoBehaviour
{
    public float AngularSpacingDegrees = 20;
    public float Radius = 2f;

	void Update () {
        var transforms = transform.Cast<Transform>().ToList();
        Util.ArrangeInArc(transforms, transform, Radius, AngularSpacingDegrees);
    }
}
