﻿using UnityEngine;
using System.Collections;

public class RatchetMenu : MonoBehaviour
{
    public States state = States.Hidden;
    private float tAtHidden;
    private float tAtActivating;
    private float tAtActive;
    private float tAtDeactivating;

    public float PitchToUnhide = 35f;
    public float CamPitchCoef = 1f;
    public float CamPitchToTriggerDeActivation = 20f;
    public float PitchToTriggerActivation = 40f;
    public float TriggerAnimSecs = 0.5f;
    public float PitchWhenActiveDegrees = 45f;

    public float CamPitch;

    private Vector3 menuDirection;
    private bool isDismissed;

    private GameObject menu;

    void Awake()
    {
        menu = transform.Find("Menu").gameObject;
    }

    void onEnable() => isDismissed = false;

    private float menuPitch;

    void UpdatePitch(float targetPitch)
    {
        menuPitch += (targetPitch - menuPitch)*5*Time.deltaTime;
    }

    private float TargetPitch => CamPitch > 0 && CamPitch < 90 ? Mathf.Pow(CamPitch / 45, 3)*90*CamPitchCoef : 0f;

    void Update()
    {
        var cam = Camera.main;
        transform.rotation = Quaternion.LookRotation(menuDirection, Vector3.up);
        CamPitch = cam.transform.rotation.eulerAngles.x;

        switch (state)
        {
            case States.Hidden:
            {
//                if (camPitch < PitchToUnhide)
//                {
//                    isDismissed = false;
//                    menu.SetActive(true);
//                }

                if (isDismissed)
                {
                    menu.SetActive(false);
                    return;
                }

                menuDirection = Vector3.ProjectOnPlane(cam.transform.forward, Vector3.up);

                UpdatePitch(TargetPitch);
                RotateMenu(menuPitch);
                if (menuPitch > PitchToTriggerActivation)
                {
                    state = States.Activating;
                    tAtActivating = Time.time;
                }
            }
                break;

            case States.Activating:
            {
                var dT = Time.time - tAtActivating;
                var progress = dT/TriggerAnimSecs;


//                menuPitch = Mathf.Lerp(PitchToTriggerActivation, PitchWhenActiveDegrees, Mathf.Sin(progress * Mathf.PI * 0.5f));
                menuPitch = Mathf.Lerp(PitchToTriggerActivation, PitchWhenActiveDegrees,
                    Mathf.Sin(progress*Mathf.PI*0.6f));

                RotateMenu(menuPitch);

                if (progress > 1)
                {
                    state = States.Active;
                    tAtActive = Time.time;
                }
            }
                break;

            case States.Deactivating:
            {
                var dT = Time.time - tAtDeactivating;
                var progress = Mathf.Min(dT/TriggerAnimSecs, 1);

                UpdatePitch(Mathf.Lerp(PitchWhenActiveDegrees, TargetPitch, 1 - Mathf.Cos(progress * Mathf.PI * 0.5f)));
                RotateMenu(menuPitch);

                if (progress >= 1)
                {
                    state = States.Hidden;
                    tAtHidden = Time.time;
                }
            }
                break;
            case States.Active:
            {
                UpdatePitch(PitchWhenActiveDegrees);
                RotateMenu(menuPitch);
                if (CamPitch < CamPitchToTriggerDeActivation)
                {
                    state = States.Deactivating;
                    tAtDeactivating = Time.time;
                }
            }
                break;

            default:
                break;
        }
    }

    public void Dismiss()
    {
        state = States.Deactivating;
        tAtDeactivating = Time.time;
        isDismissed = true;
    }

    private void RotateMenu(float menuPitch)
    {
        transform.Rotate(Vector3.left, menuPitch - 90);
    }

    public enum States
    {
        Hidden,
        Activating,
        Deactivating,
        Active
    }
}