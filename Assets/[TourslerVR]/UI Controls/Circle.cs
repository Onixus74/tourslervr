﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

[ExecuteInEditMode]
public class Circle : CircularOutline
{
    private List<Vector3> normalizedVerts;

    protected override void CreateGeometry(List<int> segs)
    {
        if (normalizedVerts == null || !segmentCount.Equals(_segmentCount))
        {
            normalizedVerts = new List<Vector3>();

            normalizedVerts.AddRange(segs.Select(segToVec));
            normalizedVerts.Add(segToVec(0));
        }

        line.SetVertexCount(segmentCount + 1);
        line.SetPositions(normalizedVerts.Select(v => v * radius).ToArray());
    }
}
