﻿using UnityEngine;
using System.Collections;

public abstract class AbstractGazeButton : MonoBehaviour {
    States state = States.Idle;
    public float SecondsToEngage { get; set; }
    public float SecondsToDisappear { get; set; }

    private float tAtIdle;
    private float tAtFocus;
    private float tAtEngage;

    // Update is called once per frame
    void Update () {
        var t = Time.time;
        
        switch (state)
        {
            case States.Idle:
                updateIdle(t - tAtIdle);
                break;
            case States.Focused:
                updateFocused(t - tAtFocus);
                break;
            case States.Engaged:
               updateEngaged(t - tAtEngage);
                break;
            default:
                break;
        }
    }

    public void GazeStart()
    {
        state = States.Focused;
        tAtFocus = Time.time;
    }

    public void GazeEnd()
    {
        state = States.Idle;
        tAtIdle = Time.time;
    }

    public abstract void updateIdle(float secsSinceIdle);
    public abstract void updateFocused(float secsSinceFocused);
    public abstract void updateEngaged(float secsSinceFocused);

    public abstract void onIdle();
    public abstract void onFocus();
    public abstract void onEngage();
    
    enum States
    {
        Idle,
        Focused,
        Engaged
    };
}
