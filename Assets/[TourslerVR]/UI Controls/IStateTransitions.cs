﻿using System;
using System.Collections.Generic;
using System.Linq;
using RSG;
using UnityEngine;
using static Util;

public interface IStateTransitions
{
    Enum State { get; }
    IPromise CurrentTransition { get; set; }
    bool IsDisabled { get; set; }
    bool IsCurrentTransitionCancelled { get; set; }
}

public static class StateTransitionsEx
{
    // TODO: move to abstract impl 
//    public static void OnDisable( this IStateTransitions st )
//    {
//        st.IsDisabled = true;
//    }
//
//    public static void OnEnable( this IStateTransitions st )
//    {
//        st.IsDisabled = false;
//    }

    static void ExceptionHandler(Exception e)
    {
        if (e.GetType() != typeof(ActionInterruptedException))
        {
            Debug.Log(e);
        }
    }

    static Action<float, Promise> CancellableIteratee(this IStateTransitions st, Action<float> iteratee)
    {
        var currState = st.State;
        return (progress, promise) =>
        {
            if (currState.Equals(st.State) && !st.IsDisabled/*TODO: Disabled should just be a state*/ && !st.IsCurrentTransitionCancelled)
            {
                iteratee(progress);
            }
            else
            {
                if(st.IsCurrentTransitionCancelled) Util.Log("cancelled", st.State);
                st.IsCurrentTransitionCancelled/*TODO: PromiseState.Cancelled*/ = false;
                if (promise.CurState.Equals(PromiseState.Pending)) promise.Reject(new ActionInterruptedException());
            }
        };
    }

    static Func<float, bool> IdleAnimationIteratee(this IStateTransitions st, Func<float, bool> iteratee) => 
        progress => !st.IsDisabled && iteratee(progress);

    public static void CancelCurrentTransition(this IStateTransitions st) => st.IsCurrentTransitionCancelled = true;

    public static void SetCurrentTransition(this IStateTransitions st, float[] durations, Action then, params Action<float>[] iteratees)
    {
        var i = 0;
        var animations = iteratees.Select(iteratee =>
        {
            var duration = durations[i++];
            return PromisifyCurried(Animate, duration, CancellableIteratee(st, iteratee));
        });

        st.CurrentTransition = Promise.Sequence(animations)
            .Then(then)
            .Catch(ExceptionHandler);
    }

    public static void SetCurrentTransition(this IStateTransitions st, float duration, Action then, Action<float> iteratee)
    {
        st.CurrentTransition = Promisify(Animate, duration, CancellableIteratee(st, iteratee))
            .Then(then)
            .Catch(ExceptionHandler);
    }

    public static void EnqueueTransition(this IStateTransitions st, float duration, Action then, Action<float> iteratee) => 
        st.CurrentTransition.Then(() => SetCurrentTransition(st, duration, then, iteratee));

    public static void SetCurrentIdleAnimation(this IStateTransitions st, Func<float, bool> iteratee)
    {
        st.CurrentTransition = AnimateIndefinitely(IdleAnimationIteratee(st, iteratee));
    }

    public static void EnqueueIdleAnimation(this IStateTransitions st, Func<float, bool> iteratee) =>
        st.CurrentTransition.Then(() => SetCurrentIdleAnimation(st, iteratee));
}