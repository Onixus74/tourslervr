﻿using UnityEngine;

public abstract class CircularOutline : Circular
{
    protected LineRenderer line;

    void Awake()
    {
        line = gameObject.GetComponent<LineRenderer>();
        line.useWorldSpace = false;
    }
}
