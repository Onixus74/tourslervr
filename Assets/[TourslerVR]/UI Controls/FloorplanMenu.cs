﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class FloorplanMenu : MonoBehaviour
{
    private int floorplansHashCode;

    private List<Transform> floorTransforms = new List<Transform>();
    private Dictionary<Scene, LinkDisc> scenesToLinkDiscs;
    private Dictionary<Floor, GameObject> floorsToGameObjects;
    private Transform spareLink;
    private GameObject container;
    private Transform exitButton;

    public float Scale = 9700f;
    public float SpacingDegrees = 60;
    public float Radius = 8.5f;

    private void Awake()
    {
        container = new GameObject();
        container.transform.SetParent(transform);

        exitButton = ((GameObject)Instantiate(Resources.Load("ExitMenu"))).transform;
        exitButton.localPosition = new Vector3(0, -3.5f, 3.5f);
        exitButton.rotation = Quaternion.Euler(30, 0, 0);
        exitButton.SetParent(transform);
        exitButton.GetComponent<LinkDisc>().OnEngage.AddListener(() => Tour.Instance.HideFloorplanMenu());
        exitButton.gameObject.SetActive(false);
    }

    public void Show(IEnumerable<Floor> floors, IEnumerable<Photo> photos, Scene currScene)
    {   
        var hash = floors.GetHashCode();
        if (!floorplansHashCode.Equals(hash))
        {
            floorplansHashCode = hash;
            container.transform.Clear();

            floorTransforms = new List<Transform>();
            scenesToLinkDiscs = new Dictionary<Scene, LinkDisc>();
            floorsToGameObjects = new Dictionary<Floor, GameObject>();

            foreach (var floor in floors)
            {
                var floorObj = (GameObject)Instantiate(Resources.Load("Floorplan"));           
                floorsToGameObjects.Add(floor, floorObj);
                floorObj.transform.SetParent(transform);

                floorObj.GetComponentInChildren<Text>().text = floor.name.ToUpper();

                var labelContainer = floorObj.transform.Find("LabelContainer");
                labelContainer.localPosition = new Vector3(0, floor.texture.height / 2f * 0.001f + 0.02f, 0);
                labelContainer.localScale = 0.3f*Vector3.one;

                var quad = floorObj.transform.Find("Quad");
                var material = quad.GetComponent<MeshRenderer>().material;
                AssetLoader.LoadFloorplanTexture(floor, material, quad.transform);
                floorObj.transform.localScale = (Scale / (Mathf.Max(floor.texture.width, floor.texture.height))) * Vector3.one;
                floorTransforms.Add(floorObj.transform);

                Func<Photo, bool> filter = p => !string.IsNullOrEmpty(p.caption) &&
                                                p.sceneObj != null &&
                                                !scenesToLinkDiscs.ContainsKey(p.sceneObj) &&
                                                floor.floorId.Equals(p.floor);

                foreach (var photo in photos.Where(filter))
                {
                    var linkTransform = CreateLink(floor, photo.sceneObj);
                    if (null != linkTransform) InitLink(linkTransform, photo.sceneObj);
                }
            }
        }

        foreach (var pair in scenesToLinkDiscs)
        {
            if(!pair.Key.Equals(currScene)) pair.Value.Init();
        }

        // YOU ARE HERE
        if (scenesToLinkDiscs.ContainsKey(currScene))
        {
            scenesToLinkDiscs[currScene].SetAsMarker();
        }
        else
        { // "spare link"
            var floor = floors.FirstOrDefault(f => f.floorId.Equals(currScene.floor));
            if (null != floor)
            {
                spareLink = CreateLink(floor, currScene);
                spareLink?.GetComponent<LinkDisc>().SetAsMarker();
            }
        }

        container.SetActive(true);

        foreach (var floor in floorTransforms)
        {
            floor.SetParent(container.transform);
        }

        var cam = Camera.main.transform;
        transform.rotation = Quaternion.LookRotation(Vector3.ProjectOnPlane(cam.forward, Vector3.up), Vector3.up);

        StartCoroutine(ArrangeFloors(container.transform, Time.time, floorTransforms, 0));

        exitButton.gameObject.SetActive(true);
    }

    private const float sceneScale = 0.001f;
    private Transform CreateLink(Floor floor, Scene scene)
    {
        var sceneX = (-floor.x + scene.x) / floor.scale * sceneScale;
        var sceneY = (-floor.y + scene.y) / floor.scale * sceneScale;

        var floorEdgeX = floor.texture.width * sceneScale;
        var floorEdgeY = -floor.texture.height * sceneScale;

        // crop links at edges
        if (sceneX < 0 || sceneX > floorEdgeX || sceneY > 0 || sceneY < floorEdgeY) return null;

        var linkTransform = ((GameObject)Instantiate(Resources.Load("Link"), Vector3.zero, Quaternion.identity)).transform;

        linkTransform.SetParent(floorsToGameObjects[floor].transform);
        linkTransform.localScale = 0.04f * Vector3.one;
        linkTransform.localRotation = Quaternion.identity;


        var floorX0 = -floor.texture.width / 2f;
        var floorY0 = floor.texture.height / 2f;

        /* if you look in the config file, for each floor it indicates a position and a scale. 
                     * The position is the lower left corner of the floorplan image, in the tour coordinate space. 
                     * The scale is (cm in the tour coordinate system) / (pixel of the floor plan image)*/
        linkTransform.localPosition = new Vector3(
            floorX0 * sceneScale + sceneX,
            floorY0 * sceneScale + sceneY,
            -0.07f);

        var dist = (linkTransform.position - Camera.main.transform.position).magnitude;
        var r = Mathf.Pow(dist, App.Instance.LinkCollisionRadiusExponent * 5f);
        linkTransform.GetComponentInChildren<SphereCollider>().radius = r;

        return linkTransform;
    }

    private void InitLink(Transform linkTransform, Scene scene)
    {
        UnityAction onLinkEngage = () =>
        {
            Tour.Instance.HideFloorplanMenu(true);
            Tour.Instance.TeleportToScene(scene, true);
        };

        var linkDisc = linkTransform.GetComponent<LinkDisc>();
        linkDisc.OnEngage.AddListener(onLinkEngage);

        linkTransform.gameObject.SetActive(true);
        scenesToLinkDiscs.Add(scene, linkDisc);
    }

    void Update()
    {
#if UNITY_EDITOR
        if (!isAnimating) ArrangeFloorsInArc(container.transform, SpacingDegrees, floorTransforms, 0);
#endif

    }

    private bool isAnimating;

    IEnumerator ArrangeFloors(Transform transform, float t, IEnumerable<Transform> floors, float y)
    {
        isAnimating = true;
        while (Time.time - t <= 0.4f)
        {
            var dT = Time.time - t;
            var progress = Mathf.Min(dT / 0.4f, 1);
            ArrangeFloorsInArc(transform, SpacingDegrees * progress, floors, y);
            yield return null;
        }
        isAnimating = false;
    }

    void ArrangeFloorsInArc(Transform transform, float spacing, IEnumerable<Transform> floors, float y)
    {
        Util.ArrangeInArc(floors, transform, Radius, spacing);
        foreach (var floor in floors)
        {
            floor.gameObject.SetActive(true);
            floor.Translate(0, y, 0);
        }
    }

    public void Hide()
    {
        if (null != spareLink)
        {
            Destroy(spareLink.gameObject);
            spareLink = null;
        }
        container.SetActive(false);
        exitButton.gameObject.SetActive(false);
    }
}