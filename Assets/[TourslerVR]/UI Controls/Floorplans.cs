﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Floorplans : MonoBehaviour
{
    void Awake()
    {
        transform.Clear();
    }

    public void Load(IEnumerable<Floor> floors)
    {
        transform.Clear();

        foreach (var floor in floors)
        {
            var floorObj = (GameObject)Instantiate(Resources.Load("Floorplan"));
            floorObj.transform.SetParent(transform);
            floorObj.GetComponentInChildren<Text>().text = floor.name.ToUpper();

            var quad = floorObj.transform.Find("Quad");
            var material = quad.GetComponent<MeshRenderer>().material;
            AssetLoader.LoadFloorplanTexture(floor, material, quad.transform);
        }

        var transforms = transform.Cast<Transform>().ToList();
        Util.ArrangeInArc(transforms, transform, 1.5f, 35);
    }
}