﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Assertions.Comparers;
using UnityEngine.Events;

[ExecuteInEditMode]
public class SquareMenuItem : MonoBehaviour
{
    public string Label = "MENU ITEM";
    public float PopForwardDistance = 0.1f;
    public float SecondsToPopForward = 0.5f;
    public float SecondsToPopBack = 0.8f;
    public float SecondsToDisappear = 0.2f;


    States state = States.Idle;
    private GameObject disc;

    private float tAtIdle;
    private float tAtFocus;
    private float tAtEngage;

    private Vector3 posAtIdle;

    private static bool isEngageTriggered;
    private static bool someoneHasFocus;

    public UnityEvent OnEngage = new UnityEvent();


    // Use this for initialization
    void Start()
    {
        disc = transform.Find("Disc").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        var t = Time.time;
        var popForwardPos = new Vector3(0, 0, -PopForwardDistance);

        switch (state)
        {
            case States.Idle:
                var dIdle = (t - tAtIdle)/SecondsToPopBack;
                disc.transform.localPosition = Vector3.Lerp(posAtIdle, Vector3.zero, dIdle);
                setTextAlpha(disc.transform, "Label", 0.7f);

                break;
            case States.Focused:
                var dFocus = Mathf.Min((t - tAtFocus) / SecondsToPopForward, 1);
                disc.transform.localPosition = Vector3.Lerp(Vector3.zero, popForwardPos, dFocus);

                setTextAlpha(disc.transform, "Label", 0.7f + 0.3f * dFocus);

                if (isEngageTriggered)
                {
                    isEngageTriggered = false;
                    someoneHasFocus = false;
                    state = States.Engaged;
                    tAtEngage = t;
                }

                break;
            case States.Engaged:
                var dDisappear = (t - tAtEngage) / SecondsToDisappear;
                disc.transform.localPosition = Vector3.Lerp(popForwardPos, Vector3.zero, dDisappear);
                setTextAlpha(disc.transform, "Label", 1f - 0.3f * dDisappear);

                if (dDisappear >= 1)
                {
                    transform.GetComponentInParent<RatchetMenu>().Dismiss();
                    state = States.Idle;
                    tAtIdle = Time.time;
                    OnEngage.Invoke();
                }
                break;
            default:
                break;
        }

        setText(disc.transform, "Label", Label);
    }

    public static void Engage()
    {
        // only want to trigger if a disc is in focus
        isEngageTriggered = someoneHasFocus;
    }

    public void GazeStart()
    {
        state = States.Focused;
        tAtFocus = Time.time;
        someoneHasFocus = true;
    }

    public void GazeEnd()
    {
        state = States.Idle;
        tAtIdle = Time.time;
        posAtIdle = disc.transform.localPosition;

        someoneHasFocus = false;
        isEngageTriggered = false;
    }

    enum States
    {
        Idle,
        Focused,
        Engaged
    };

    private static void setText(Transform transform, string objName, string text)
    {
        transform.Find(objName).Find("Text").GetComponent<Text>().text = text;
    }

    private static void setTextAlpha(Transform transform, string objName, float alpha)
    {
        var color = transform.Find(objName).Find("Text").GetComponent<Text>().color;
        transform.Find(objName).Find("Text").GetComponent<Text>().color = new Color(color.r, color.g, color.b, alpha);
    }
}
