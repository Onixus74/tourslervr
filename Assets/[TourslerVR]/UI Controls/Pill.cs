﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

[ExecuteInEditMode]
public class Pill : CircularFill
{
    public float leafWidth;
    private float _leafWidth;

    private List<Vector3> normalizedVerts = new List<Vector3>();

    protected override void CreateGeometry(List<int> segs)
    {
        if (!segmentCount.Equals(_segmentCount))
        {
            normalizedVerts.Clear();
            normalizedVerts.AddRange(segs.Take(segmentCount / 2).Select((seg) => segToVec(seg) + new Vector3(leafWidth / 2, 0, 0)));
            normalizedVerts.AddRange(segs.Skip(segmentCount / 2).Select((seg) => segToVec(seg) + new Vector3(-leafWidth / 2, 0, 0)));
            normalizedVerts.Add(segToVec(0) + new Vector3(leafWidth / 2, 0, 0));
        }

        var uvs = normalizedVerts.Select((v) => (Vector2.one + new Vector2(leafWidth, 0))/2 + new Vector2(v.x, v.y)/2 * (cropTexture ? radius : 1));

        AssignGeometryToMesh(segs, normalizedVerts, uvs);
    }

    protected new bool IsUpdateNeeded() => !leafWidth.Equals(_leafWidth) || base.IsUpdateNeeded();

    protected new void Update()
    {
        if (segmentCount % 2 != 0) segmentCount++;
        base.Update();
        _leafWidth = leafWidth;
    }
}