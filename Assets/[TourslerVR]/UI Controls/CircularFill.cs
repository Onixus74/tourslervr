﻿using System.Collections.Generic;
using UnityEngine;
using static System.Linq.Enumerable;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public abstract class CircularFill : Circular
{
    public bool cropTexture;
    protected bool _cropTexture;

    protected Mesh mesh;

    void Awake()
    {
        mesh = new Mesh();
        gameObject.GetComponent<MeshFilter>().mesh = mesh;
    }

    private List<int> tris;

    protected void AssignGeometryToMesh(List<int> segs, List<Vector3> normalizedVerts, IEnumerable<Vector2> uvs)
    {
        if(!segmentCount.Equals(_segmentCount)) mesh.Clear();

        mesh.vertices = normalizedVerts.Select(v => v * radius).ToArray();
        mesh.uv = uvs.ToArray();

        if (tris == null || !segmentCount.Equals(_segmentCount))
        {
            tris = new List<int>();
            foreach (var seg in segs)
            {
                tris.Add(segmentCount); // center
                tris.Add(seg);
                tris.Add((seg + 1) % segmentCount);
            }
            mesh.triangles = tris.ToArray();
        }
    }

    protected new bool IsUpdateNeeded() => cropTexture != _cropTexture || base.IsUpdateNeeded();
}
