﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[ExecuteInEditMode]
public class Underline : MonoBehaviour
{
    private RectTransform textRect;
    private Text textTextComponent;
    private RectTransform underlineRect;
    private Text underlineTextComponent;

    // Use this for initialization
    void Start () {
        textRect = gameObject.GetComponent<RectTransform>();
        textTextComponent = gameObject.GetComponent<Text>();
        
        underlineRect = gameObject.transform.Find("Underline").GetComponent<RectTransform>();
        underlineTextComponent = gameObject.transform.Find("Underline").GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        underlineRect.sizeDelta = new Vector2(textRect.rect.width, underlineRect.rect.height);
        // * 2 is totally a hack to let "best size" scale the font better since Unity's font sizing algorithm is totally unclear
        underlineTextComponent.text = new string('_', textTextComponent.text.Length * 2);
	}
}
