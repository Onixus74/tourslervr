﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

[ExecuteInEditMode]
public class Disc : CircularFill
{
    private List<Vector3> normalizedVerts = new List<Vector3>();

    protected override void CreateGeometry(List<int> segs)
    {
        if (!segmentCount.Equals(_segmentCount))
        {
            normalizedVerts.Clear();
            normalizedVerts.AddRange(segs.Select(segToVec));
            normalizedVerts.Add(Vector3.zero);
        }

        var uvs = normalizedVerts.Select((v) => Vector2.one / 2 + new Vector2(v.x, v.y) / 2 * (cropTexture ? radius : 1));
        AssignGeometryToMesh(segs, normalizedVerts, uvs);

        _cropTexture = cropTexture;
    }
}