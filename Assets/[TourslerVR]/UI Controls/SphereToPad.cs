﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

public class SphereToPad : MonoBehaviour {

	// Use this for initialization   
	void Start () {
	
	}

    private float t0 = 0;
    private bool go = false;

    private Vector3 scale; 
    private Vector3 pos; 

	// Update is called once per frame
	void Update ()
	{
	    var t = Time.time;

        if (Input.GetKeyDown(KeyCode.Space))
	    {
	        t0 = t;
	        go = true;
	       
	    }

	    if (!go)
	    {
	        transform.position = new Vector3(0, 0.5f + Mathf.Sin(t*Mathf.PI*2)/16f, 0);
	    }
	    else
	    {
            transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(1, 0.2f, 1), Mathf.Min(1, t - t0));
            transform.localPosition = Vector3.Lerp(transform.localPosition, Vector3.zero, Mathf.Min(1, t - t0));
        }


    }
}
