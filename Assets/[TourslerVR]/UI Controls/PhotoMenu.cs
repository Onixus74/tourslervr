﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class PhotoMenu : MonoBehaviour
{
    private int photosHashCode;

    private List<List<Transform>> pagesOfLinks = new List<List<Transform>>();
    private const float arclen = 20;
    private const float radius = 5;
    private const int pageSize = 6;
    private int currPageIdx;
    private int nextPageIdx => (currPageIdx + 1)%pagesOfLinks.Count;
    private int prevPageIdx => (pagesOfLinks.Count + currPageIdx - 1)%pagesOfLinks.Count;
    private List<Transform> currentPage => pagesOfLinks[currPageIdx];
    private List<Transform> nextPage => pagesOfLinks[nextPageIdx];
    private List<Transform> prevPage => pagesOfLinks[prevPageIdx];

    private GameObject pageContainerA;
    private GameObject pageContainerB;
    private bool aOrB;
    private GameObject currContainer => aOrB ? pageContainerA : pageContainerB;
    private GameObject nextContainer => !aOrB ? pageContainerA : pageContainerB;

    private GameObject nextLink;
    private GameObject prevLink;

    private Transform exitButton;

    private void Awake()
    {
        pageContainerA = new GameObject();
        pageContainerB = new GameObject();
        pageContainerA.transform.SetParent(transform);
        pageContainerB.transform.SetParent(transform);

        nextLink = (GameObject)Instantiate(Resources.Load("Link"), Vector3.zero, Quaternion.identity);
        prevLink = (GameObject)Instantiate(Resources.Load("Link"), Vector3.zero, Quaternion.identity);
        nextLink.transform.SetParent(transform);
        prevLink.transform.SetParent(transform);
        nextLink.SetActive(false);
        prevLink.SetActive(false);

        var nextLinkDisc = nextLink.GetComponent<LinkDisc>();
        var prevLinkDisc = prevLink.GetComponent<LinkDisc>();
        nextLinkDisc.Breathe = false;
        prevLinkDisc.Breathe = false;
        nextLinkDisc.OnEngage.AddListener(() => NextPage());
        prevLinkDisc.OnEngage.AddListener(() => PrevPage());
        nextLinkDisc.Label = "NEXT";
        prevLinkDisc.Label = "PREV";
        

        var transforms = new List<Transform>() { prevLink.transform, nextLink.transform };
        Util.ArrangeInArc(transforms, transform, radius, 4 * arclen);

        exitButton = ((GameObject)Instantiate(Resources.Load("ExitMenu"))).transform;
        exitButton.localPosition = new Vector3(0, -3.5f, 3.5f);
        exitButton.rotation = Quaternion.Euler(30, 0, 0);
        exitButton.SetParent(transform);
        exitButton.GetComponent<LinkDisc>().OnEngage.AddListener(() => Tour.Instance.HidePhotoMenu());
        exitButton.gameObject.SetActive(false);
    }

    public void Show(IEnumerable<Photo> photos)
    {
        var hash = photos.GetHashCode();
        if (!photosHashCode.Equals(hash))
        {
            photosHashCode = hash;
            currPageIdx = 0;
            currContainer.transform.Clear();
            nextContainer.transform.Clear();

            var links = new List<Transform>();
            foreach (var photo in photos)
            {
                var link = ((GameObject)Instantiate(Resources.Load("PhotoLink"), Vector3.zero, Quaternion.identity)).transform;
                var linkDisc = link.GetComponent<LinkDisc>();
                linkDisc.Label = photo.caption.ToUpper();
                linkDisc.Breathe = false;
                link.SetParent(transform);
                var material = link.Find("Disc").GetComponent<MeshRenderer>().material;
                material.SetTexture("_MainTex", photo.texture);

                UnityAction onLinkEngage = () =>
                {
                    Tour.Instance.HidePhotoMenu(true);
                    Tour.Instance.TeleportToScene(photo.sceneObj, true);
                };

                link.GetComponent<LinkDisc>().OnEngage.AddListener(onLinkEngage);
                link.gameObject.SetActive(false);

                links.Add(link);
            }

            pagesOfLinks = links.InSetsOf(pageSize).ToList();
        }

        if (pagesOfLinks.Count > 1)
        {
            nextLink.SetActive(true);
            prevLink.SetActive(true);
        }
        currContainer.SetActive(true);

        foreach (var link in currentPage)
        {
            link.SetParent(currContainer.transform);
        }

        var cam = Camera.main.transform;
        transform.rotation = Quaternion.LookRotation(Vector3.ProjectOnPlane(cam.forward, Vector3.up), Vector3.up);

        StartCoroutine(ArrangeLinks(currContainer.transform, Time.time, currentPage.Take(3), 0.75f));
        StartCoroutine(ArrangeLinks(currContainer.transform, Time.time, currentPage.Skip(3), -0.75f));
        exitButton.gameObject.SetActive(true);
    }

    public void PrevPage()
    {
        if (!currContainer.activeSelf) return; // prevent photo menu from showing when swiping left/right on floorplan menu
        foreach (var link in prevPage)
        {
            link.SetParent(nextContainer.transform);
        }

        nextContainer.transform.localRotation = Quaternion.identity;
        ArrangeLinksInArc(nextContainer.transform, arclen, prevPage.Take(3), 0.75f);
        ArrangeLinksInArc(nextContainer.transform, arclen, prevPage.Skip(3), -0.75f);

        StartCoroutine(Rotate(nextContainer.transform, arclen * -3, 0, Time.time));
        StartCoroutine(RotateThenVanishContainer(currContainer, 0, arclen * 3, Time.time));

        currPageIdx = prevPageIdx;
        aOrB = !aOrB;
    }

    public void NextPage()
    {
        if (!currContainer.activeSelf) return; // prevent photo menu from showing when swiping left/right on floorplan menu
        foreach (var link in nextPage)
        {
            link.SetParent(nextContainer.transform);
        }

        nextContainer.transform.localRotation = Quaternion.identity;
        ArrangeLinksInArc(nextContainer.transform, arclen, nextPage.Take(3), 0.75f);
        ArrangeLinksInArc(nextContainer.transform, arclen, nextPage.Skip(3), -0.75f);

        StartCoroutine(Rotate(nextContainer.transform, arclen * 3, 0, Time.time));
        StartCoroutine(RotateThenVanishContainer(currContainer, 0, arclen * -3, Time.time));

        currPageIdx = nextPageIdx;
        aOrB = !aOrB;
    }

    IEnumerator RotateThenVanishContainer(GameObject container, float start, float end, float t)
    {
        yield return Rotate(container.transform, start, end, t);
        container.transform.TransferAndDeactivateChildren(transform);
    }

    IEnumerator Rotate(Transform transform, float start, float end, float t)
    {
        while (Time.time - t <= 0.4f)
        {
            var dT = Time.time - t;
            var progress = Mathf.Min(dT / 0.4f, 1);
            transform.localRotation = Quaternion.Euler(0, Mathf.Lerp(start, end, progress), 0);
            yield return null;
        }
    }

    IEnumerator ArrangeLinks(Transform transform, float t, IEnumerable<Transform> links, float y)
    {
        while (Time.time - t <= 0.4f)
        {
            var dT = Time.time - t;
            var progress = Mathf.Min(dT / 0.4f, 1);
            ArrangeLinksInArc(transform, arclen * progress, links, y);
            yield return null;
        }
        
    }

    void ArrangeLinksInArc(Transform transform, float spacing, IEnumerable<Transform> links, float y)
    {
        Util.ArrangeInArc(links, transform, radius, spacing);
        foreach (var link in links)
        {
            link.gameObject.SetActive(true);
            link.Translate(0, y, 0);
        }
    }

    public void Hide()
    {
        currContainer.SetActive(false);
        nextLink.SetActive(false);
        prevLink.SetActive(false);
        exitButton.gameObject.SetActive(false);
    }
}