﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine.UI;

[ExecuteInEditMode]
public class ListingDetails : MonoBehaviour
{
    //[SerializeField] //use this when turning these into private fields
    public bool IsFeatured;
    public string Broker;
    public string Address;
    public string City;
    public List<Datum> Data;
    public float AngularSpacingDegrees = 10;
    public float Radius = 3.5f;

	// Use this for initialization
	void Start ()
	{
	    
	}
	
	// Update is called once per frame
	void Update ()
	{
	    var titlePivot = transform.Find("TitleContainerPivot");
	    var titleContainer = titlePivot.Find("TitleContainer");
        var badge = titleContainer.Find("Badge");
        setText(badge, "Label", IsFeatured ? "FEATURED LISTING" : "VIEWED RECENTLY ON THIS DEVICE");
        setText(titleContainer, "Broker", Broker);
        setText(titleContainer, "Address", Address);
        setText(titleContainer, "City", City);

        var dataContainer = transform.Find("DataContainer");
        if(!dataContainer.childCount.Equals(Data.Count))
        {
            dataContainer.transform.Clear();

            foreach (var datum in Data)
            {
                var datumGameObj = (GameObject)Instantiate(Resources.Load("Datum"));
                datumGameObj.transform.SetParent(dataContainer.transform, false);
            }
	    }
        
	    var i = 0;
        foreach (var datum in Data)
        {
            var datumGameObj = dataContainer.GetChild(i).gameObject;
            setText(datumGameObj.transform, "SerifLabel", datum.val);
            setText(datumGameObj.transform, "Label", datum.label);
            i++;
        }

        var dataTransforms = dataContainer.Cast<Transform>().ToList();
        Util.ArrangeInArc(dataTransforms, dataContainer, Radius, AngularSpacingDegrees);
    }
    
    static void setText(Transform transform, string objName, string text)
    {
        transform.Find(objName).Find("Text").GetComponent<Text>().text = text;
    }
}

[System.Serializable]
public class Datum
{
    public Datum(string label, object val)
    {
        this.label = label;
        this.val = val.ToString();
    }

    public string val;
    public string label;
}

