﻿using System;
using System.Collections.Generic;
using RSG;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

[ExecuteInEditMode]
public class LinkDisc : MonoBehaviour, IStateTransitions
{
    // State Transitions
    public States state = States.Init;
    public Enum State => state;
    public IPromise CurrentTransition { get; set; }
    public bool IsCurrentTransitionCancelled { get; set; }
    public bool IsDisabled { get; set; }

    // Props
    public string Label = "";
    public float InitAnimationSeconds = 0.6f;
    public Color Color = new Color(1, 1, 1, 0.7f);
    public Color MarkerColor = new Color(1, 0, 0);
    public float Radius = 0.5f;
    public bool Breathe = true;
    public float DiscBreatheHz = 0.5f;
    public float DiscBreatheProportionOfRadius = 0.1f;
    public float CircleExpandProportionOfRadius = 0.2f;
    public float CircleExpandSeconds = 0.3f;
    public float DiscShrinkToProportionOfRadius = 0.7f;
    public float DiscExpandSeconds = 1.5f;
    public float DisappearSeconds = 0.3f;
    public float PillShrinkAspectRatio = 1f;
    public UnityEvent OnEngage = new UnityEvent();

    private float ExpandedDiscRadius => Radius + Radius*CircleExpandProportionOfRadius;
    private float ShrunkenDiscRadius => Radius*DiscShrinkToProportionOfRadius;

    private GameObject discObj;
    private CircularFill disc;
    private CircularOutline circle;

    private static LinkDisc discWithFocus;

    private GameObject audio;
    private Dictionary<string, Transform> audioSources = new Dictionary<string, Transform>(); 

    void Awake()
    {
        discObj = transform.Find("Disc").gameObject;
        disc = discObj.GetComponent<CircularFill>();
        circle = transform.Find("Circle").gameObject.GetComponent<CircularOutline>();
        audio = GameObject.Find("Audio");
        if (null != audio)
        {
            audioSources.Add("LinkHover", audio.transform.Find("LinkHover"));
            audioSources.Add("LinkEngage", audio.transform.Find("LinkEngage"));
        } else Debug.LogWarning("Audio object not found. Should be attached to your VR camera rig");
        
        if (state != States.Marker) Init();
    }

    void OnEnable()
    {
        IsDisabled = false;
        if (state != States.Marker) Init();
    }

    void OnDisable() => IsDisabled = true;

    public void SetAsMarker() => SetState(States.Marker);

    public void Init()
    {
        SetState(States.Init);
        gameObject.SetActive(true);
        // todo add initial rotation and position
    }

    public IPromise Disable() => SetState(States.Disabled);

    void Update()
    {
        SetText(transform, "Label", Label);
        //SetDiscColor(Color); TODO: user renderer.sharedMaterial
    }

    IPromise SetState(States nextState)
    {
        if (state == States.Init && nextState > States.Idle) return CurrentTransition;
        if (state == States.Clicked && nextState.Equals(States.Clicked)) return CurrentTransition;
        if (state == States.Marker && nextState >= States.Idle) return CurrentTransition;

        state = nextState;

        switch (nextState)
        {
            case States.Disabled:
                var discRadius = disc.radius;
                var circleRadius = circle.radius;

                this.SetCurrentTransition(1f,
                    () => {gameObject.SetActive(false);},
                    progress =>
                    {
                        disc.radius = Mathf.Lerp(discRadius, 0, progress);
                        circle.radius = Mathf.Lerp(circleRadius, 0, progress);
                        SetAlphaRelativeToCamera(0.7f * (1 -progress));
                    });
                break;
            case States.Init:
                circle.radius = 0;
                disc.radius = 0;
                SetDiscColor(Color);
                SetAlphaRelativeToCamera(0);
                this.SetCurrentTransition(InitAnimationSeconds,
                    () => SetState(States.Idle),
                    progress =>
                    {
                        disc.radius = Mathf.Lerp(0, Radius, progress);
                        SetAlphaRelativeToCamera(0.7f * progress);
                    });
                break;
            case States.Marker:
                SetDiscColor(MarkerColor);
                disc.radius = Radius;
                circle.radius = 0;
                this.SetCurrentIdleAnimation(BreatheAnimation);
                break;
            case States.Idle:
                circle.radius = 0;
                if (Breathe) this.SetCurrentIdleAnimation(BreatheAnimation);
                else this.SetCurrentIdleAnimation(MenuItemIdle);
                break;
            case States.Blurred:
                discWithFocus = null;
                Blur();
                break;
            case States.Focused:
                discWithFocus = this;
                PlayAudio("LinkHover");
                FocusClickBlur();


                break;
            case States.Clicked:
                FocusClickBlur(0.04f);
                break;
            case States.Engaged:
                circle.radius = 0;
                discWithFocus = null;
                PlayAudio("LinkEngage");
                OnEngage.Invoke();

                this.SetCurrentTransition(
                    DisappearSeconds,
                    () => SetState(States.Init),
                    progress =>
                    {
                        disc.radius = Mathf.Lerp(ExpandedDiscRadius, 0, progress);
                        SetAlphaRelativeToCamera(1 - progress);
                    });
                break;
        }

        return CurrentTransition;
    }

    private bool BreatheAnimation (float dT)
    {
        SetAlphaRelativeToCamera(0.7f);
        var dRadius = DiscBreatheProportionOfRadius * Radius * Mathf.Sin(dT * DiscBreatheHz * Mathf.PI * 2);
        disc.radius = Radius + dRadius;
        return state.Equals(States.Idle) || state.Equals(States.Marker);
    }

    private bool MenuItemIdle(float dT)
    {
        SetAlphaRelativeToCamera(0.7f);
        return state.Equals(States.Idle);
    }

    void FocusClickBlur(float coef = 1)
    {
        var discRadiusStart = disc.radius;
        var circleRadiusStart = Math.Max(discRadiusStart, circle.radius);

        float discRadiusStartPartTwo = ShrunkenDiscRadius;
        this.SetCurrentTransition(
            new[] { CircleExpandSeconds * coef, DiscExpandSeconds * coef }, // durations
            () => SetState(States.Engaged), // done
            progress => // circle expand
            {
                circle.radius = Mathf.Lerp(circleRadiusStart, ExpandedDiscRadius, progress);
                disc.radius = Mathf.Lerp(discRadiusStart, ShrunkenDiscRadius, progress);
                discRadiusStartPartTwo = disc.radius;
            },
            progress => // disc expand
            {
                disc.radius = Mathf.Lerp(discRadiusStartPartTwo, ExpandedDiscRadius, progress);
                SetAlphaRelativeToCamera(0.7f + 0.3f * progress);
            });
    }

    void FocusTouchController(float coef = 1)
    {
        var discRadiusStart = disc.radius;
        var circleRadiusStart = Math.Max(discRadiusStart, circle.radius);

        float discRadiusStartPartTwo = ShrunkenDiscRadius;
        this.SetCurrentTransition(
            new[] { CircleExpandSeconds * coef }, // durations
            () => { }, // done
            progress => // circle expand
            {
                circle.radius = Mathf.Lerp(circleRadiusStart, ExpandedDiscRadius, progress);
                disc.radius = Mathf.Lerp(discRadiusStart, ShrunkenDiscRadius, progress);
                discRadiusStartPartTwo = disc.radius;
            });
    }

    void Blur()
    {
        var discRadiusStart = disc.radius;
        var circleRadiusStart = Math.Min(ExpandedDiscRadius, circle.radius);
        var coef = 0.3f;

        var p = Mathf.Abs(discRadiusStart - Radius) / (ExpandedDiscRadius - ShrunkenDiscRadius);

        this.SetCurrentTransition(
            DiscExpandSeconds * coef * p,
            () => SetState(States.Idle), // done
            progress =>
            {
                disc.radius = Mathf.Lerp(discRadiusStart, Radius, progress);
                circle.radius = Mathf.Lerp(circleRadiusStart, Radius, progress);
                SetAlphaRelativeToCamera(0.7f + 0.3f * (1 - progress));
            });
    }

    void PlayAudio(string audioSource)
    {
        if (null != audio)
        {
            var trans = audioSources[audioSource];
//            trans.position = transform.position; // no point spatializing. they're always going to be straight ahead
            trans.GetComponent<AudioSource>().Play();
        }
    }

    public static void Engage() => discWithFocus?.SetState(States.Clicked);

    private void SetAlphaRelativeToCamera(float alpha)
    {
        var dPosition = transform.position - Camera.main.transform.position;
        var angularDistance = Vector3.Angle(Camera.main.transform.forward, dPosition);
        var minAlpha = Math.Min(0.1f, alpha);
        var camAlpha = Mathf.Max(minAlpha, alpha - Mathf.Pow(angularDistance/50, 2));
        SetDiscAlpha(camAlpha);
        SetTextAlpha(transform, "Label", camAlpha);
    }

    public void GazeStart()
    {
      if (state >= States.Idle) SetState(States.Focused);
    } 

    public void GazeEnd()
    {
        if (state >= States.Idle) SetState(States.Blurred);
    }

    public enum States
    {
        Disabled,
        Init,
        Marker,
        Idle,
        Blurred,
        Focused,
        Clicked,
        Engaged,
    };

    static void SetText(Transform transform, string objName, string text)
    {
        transform.Find(objName).Find("Text").GetComponent<Text>().text = text;
    }

    private void SetDiscAlpha(float alpha)
    {
#if UNITY_EDITOR
        if (!Application.isPlaying) return;
#endif
        var color = discObj.GetComponent<MeshRenderer>().material.color;
        SetDiscColor(color.r, color.g, color.b, alpha);
    }

    public void SetDiscColor(float r, float g, float b, float alpha = 1f) => SetDiscColor(new Color(r, g, b, alpha));


    public void SetDiscColor(Color color) => discObj.GetComponent<MeshRenderer>().sharedMaterial.color = color;

    static void SetTextAlpha(Transform transform, string objName, float alpha)
    { // TODO: extention function to fade a Color (return a new Color with different alpha)
        var color = transform.Find(objName).Find("Text").GetComponent<Text>().color;
        transform.Find(objName).Find("Text").GetComponent<Text>().color = new Color(color.r, color.g, color.b, alpha);
    }
}