﻿using System.Collections.Generic;
using UnityEngine;
using static System.Linq.Enumerable;

public abstract class Circular : MonoBehaviour
{
    [Range(3, 100)]
    public int segmentCount;
    protected int _segmentCount;
    private List<int> segs;
     
    public float radius;
    protected float _radius;

    protected abstract void CreateGeometry(List<int> segs);

    void CreateGeometry()
    {
        if(segs == null || segmentCount != _segmentCount) segs = Range(0, segmentCount).ToList();
        CreateGeometry(segs);
    }

    protected Vector3 segToVec (int i)
    {
        var dTheta = 2 * Mathf.PI / segmentCount;
        var theta = dTheta * i;
        return new Vector3(
            Mathf.Sin(theta),
            Mathf.Cos(theta),
            0);
    }

    protected bool IsUpdateNeeded() => !_radius.Equals(radius) || _segmentCount != segmentCount;

    protected void Update()
    {
        if (IsUpdateNeeded()) CreateGeometry();
        _radius = radius;
        _segmentCount = segmentCount;
    }
}
