﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Pathfinding.Serialization.JsonFx;
using RSG;
using UnityEngine;
//using static System.Linq.Enumerable;

public static class AssetLoader
{
    private static readonly CubemapFace[] cubeFaces =
    {   // order matters
        CubemapFace.PositiveZ,
        CubemapFace.PositiveX,
        CubemapFace.NegativeZ,
        CubemapFace.NegativeX,
        CubemapFace.PositiveY,
        CubemapFace.NegativeY
    };

    private static string TourUrl(string tourName) => "http://toursler.com/show/config/" + tourName;
    private static string TourPath(string tourName) => tourName + "_cfg.json";

    private static string TourDetailsUrl(string tourName) => "http://real.vision/api/tour/" + tourName;
    private static string TourDetailsPath(string tourName) => tourName + "_details.json";

	private static string TextureUrl(Scene scene, int i) => "https://real.vision/asset/vrtile/" + scene.code + "/" + i ;
    private static string TexturePath(Scene scene, int i) => scene.code + "_" + i + ".jpg";

    private static string FloorplanUrl(Floor floor) => "https://real.vision/asset/floor/" + floor.floorId;
    private static string FloorplanPath(Floor floor) => floor.floorId + ".png";

    //TODO: reference DetailsPhoto from Photo
    private static string PhotoTextureUrl(Photo photo) => "https://d20gdid2ajppdc.cloudfront.net/" + photo.filename + "_small.jpg";
    private static string PhotoTexturePath(Photo photo) => photo.filename + "_small.jpg";

    private const string RecentlyViewedPath = "recentlyViewed.json";


    private static readonly bool saveToDisk = false;

    private static void StartCoroutine(IEnumerator routine) => App.Instance?.StartCoroutine(routine);

    public static IPromise LoadSceneTextures(Scene scene) => LoadSceneTextures(scene, true);

    // public static void EnqueueLoadSceneTextures(Scene scene) => LoadSceneTextures(scene, false).Done();

    private static readonly Dictionary<string, Action> downloadedTextureUrlsToLoaders = new Dictionary<string, Action>();
    private static Scene currSceneToLoadImmediately;

    private static IPromise LoadSceneTextures(Scene scene, bool loadImmediately)
    {
        if (loadImmediately) currSceneToLoadImmediately = scene;

        if (!scene.texturesQueuedForDownload)
        {
            scene.cubemap = new Cubemap(1024, TextureFormat.RGB24, false);
            scene.texturesQueuedForDownload = true;

            var promises = Enumerable.Range(0, 6).Select(i => DownloadTextureForScene(scene, i)
                .Then(www =>
                {
                    if (scene == currSceneToLoadImmediately)
                        LoadSceneTexture(scene, www, cubeFaces[i]);
                    else
                    {
                        var key = TextureUrl(scene, i);
                        if (!downloadedTextureUrlsToLoaders.ContainsKey(key))
                            downloadedTextureUrlsToLoaders.Add(TextureUrl(scene, i), () => LoadSceneTexture(scene, www, cubeFaces[i]));
                    }
                    return Promise.Resolved();
                }));

            return Promise.All(promises);
        }
        else
        {
            if (loadImmediately)
				Enumerable.Range(0, 6).ToList().ForEach(i =>
                {
                    var key = TextureUrl(scene, i);
                    if (!downloadedTextureUrlsToLoaders.ContainsKey(key)) return; // we've already loaded this texture
                    downloadedTextureUrlsToLoaders[key].Invoke();
                    downloadedTextureUrlsToLoaders.Remove(key);
                });

            return Promise.Resolved();
        }
    }

    private static void LoadSceneTexture(Scene scene, WWW www, CubemapFace face)
    {
        scene.SetTexture(www.texture, face);
        DisposeWWW(www);
    }

    public static IPromise DownloadTexturesForScene(Scene scene)
	=> Promise<WWW>.All(Enumerable.Range(0, 6).Select(i => DownloadTextureForScene(scene, i)))
        .Then(wwws =>
        {
            wwws.ToList().ForEach(DisposeWWW);
            return Promise.Resolved();
        });

//    public static IEnumerator UnzipOfflineData()
//    {
//        var progress = new int[1];
//        var progress2 = new int[1];
//        var zipFile = FullPath("TourslerVR.zip");
//        if (File.Exists(zipFile))
//        {
//            yield return lzip.decompress_File(zipFile, Application.persistentDataPath, progress, null, progress2);
//            File.Delete(zipFile);
//        }
//    }

    public static IPromise<List<TourSelectionConfig>> DownloadTourList() =>
        Fetch("http://toursler.com/content/vr_features.js", "vr_features.js")
            .Then(www =>
            {
                Debug.Log("Downloading");
                var tourList = DeserializeTourListJson(www);
                DisposeWWW(www);
                return tourList;
            });

    private static List<TourSelectionConfig> DeserializeTourListJson(WWW www)
    {
        var tourListJson = Encoding.UTF8.GetString(www.bytes);
        return JsonReader.Deserialize<TourSelectionConfig[]>(tourListJson).ToList();
    }

    public static IPromise<TourConfig> DownloadTourConfig(string tourName) =>
        Fetch(TourUrl(tourName), TourPath(tourName))
            .Then(www =>
            {
                var tourConfigJson = Encoding.UTF8.GetString(www.bytes);
                var tourConfig = JsonReader.Deserialize<TourConfig>(tourConfigJson);
                DisposeWWW(www);
                return tourConfig;
            });

    public static IPromise<TourDetails> DownloadTourDetails(string tourName) =>
        Fetch(TourDetailsUrl(tourName), TourDetailsPath(tourName))
            .Then(www =>
            {
                var tourDetailsJson = Encoding.UTF8.GetString(www.bytes);
                var tourDetails = JsonReader.Deserialize<TourDetails>(tourDetailsJson);
                DisposeWWW(www);
                return tourDetails;
            });

    public static IPromise DownloadPhotoTexture(Photo photo) =>
        Fetch(PhotoTextureUrl(photo), PhotoTexturePath(photo))
            .Then(www =>
            {
                photo.texture = www.texture;
                DisposeWWW(www);
                return Promise.Resolved();
            });

    public static IPromise DownloadFloorplanTexture(Floor floor) =>
        Fetch(FloorplanUrl(floor), FloorplanPath(floor))
            .Then(www =>
            {
                floor.texture = www.texture;
                DisposeWWW(www);
                return Promise.Resolved();
            });

    public static void LoadFloorplanTexture(Floor floor, Material material, Transform transform)
    {
        material.SetTexture("_MainTex", floor.texture);
        transform.localScale = new Vector3(floor.texture.width * 0.001f, floor.texture.height * 0.001f, 1);
    }

    private static IPromise<WWW> DownloadTextureForScene(Scene scene, int i) =>
        Fetch(TextureUrl(scene, i), TexturePath(scene, (int) cubeFaces[i]));

    private static IPromise<WWW> Fetch(string url, string relativePath, bool loadFromDisk = false)
    {
        var promise = new Promise<WWW>();
        StartCoroutine(FetchWWW(promise, url, relativePath, App.Instance.IsOfflineMode || loadFromDisk));
        return promise.Then(www =>
        {
            if (saveToDisk) WriteFile(relativePath, www);
        });
    }

    private static IEnumerator FetchWWW(IPendingPromise<WWW> promise, string url, string relativePath, bool loadFromDisk)
    {
        var www = GetWWW(url, relativePath, loadFromDisk);
        yield return www;

        if (string.IsNullOrEmpty(www.error))
        {
            Util.Log("fetched", url, relativePath);
            promise.Resolve(www);
        }
        else
        {
            Util.Log("FAILED", url, www.error);
            App.Instance.AppendSplashMessage("Error Loading. Please exit and try again.");
            promise.Reject(new Exception(www.error));
            DisposeWWW(www);
        }
    }

    private static void DisposeWWW(WWW www)
    {
        www.Dispose();
        // GameObject.DestroyImmediate(www.texture);
        // Debug.Log("destroying"+ www.data);
        Resources.UnloadUnusedAssets();
    }

    // TODO: save everything to storage; if we have the file use otherwise download it;
    // TODO: always download text files since they can become outdated but fallback to downloaded copy
    private static WWW GetWWW(string url, string relativePath, bool loadFromDisk)
    {
        //Note: When using file protocol on Windows and Windows Store Apps for accessing local files, you have to specify file:/// (with three slashes).
        var isWin = Application.platform == RuntimePlatform.WindowsPlayer ||
                    Application.platform == RuntimePlatform.WindowsEditor;
        var protocol = isWin ? "file:///" : "file://";
        return new WWW(loadFromDisk ? protocol + FullPath(relativePath) : url);
    }

    private static string FullPath(string relativePath)
        => Application.persistentDataPath + Path.DirectorySeparatorChar + relativePath;

    private static void WriteFile(string relativePath, WWW www)
    {
        File.WriteAllBytes(FullPath(relativePath), www.bytes);
    }

    public static void SaveRecentlyVisited(List<String> tourNames) => 
        File.WriteAllText(FullPath(RecentlyViewedPath), JsonWriter.Serialize(tourNames));

    public static IPromise<List<string>> LoadRecentlyVisited()
    {
        if (File.Exists(FullPath(RecentlyViewedPath)))
        {
            return Fetch(RecentlyViewedPath, RecentlyViewedPath, true)
                .Then(www =>
                {
                    var txt = www.text;
                    var strs = txt
                    .Substring(1, txt.Length - 2) // remove [ ]
                    .Split(',');

                    var recents = strs.Select(str => str.Substring(1, str.Length - 2)).ToList();

                    // TODO: Why TF isn't JsonReader.Deserialize working????
                    // var recents = JsonReader.Deserialize<List<string>>(www.text);
                    DisposeWWW(www);
                    return recents;
                });
        } else return Promise<List<string>>.Resolved(new List<string>());
    }
        
}
