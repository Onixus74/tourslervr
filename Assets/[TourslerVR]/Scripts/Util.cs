﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG;
using UnityEngine;

public static class Util
{
    const float HalfPI = Mathf.PI / 2;

    public static void Log(params System.Object[] msgs)
    {
        return;
        if(Application.isEditor)
            Debug.Log("dev only log: " + string.Join(" \n", msgs.Select(msg => msg.ToString()).ToArray()));
    }

    public static void ArrangeInArc(IEnumerable<Transform> transforms, Transform anchor, float radius, float spacingDegrees)
    {
        var numSpaces = Math.Max(transforms.Count() - 1, 0);
        var spacingRads = spacingDegrees * Mathf.Deg2Rad;
        
        var theta = spacingRads * numSpaces / 2;
        foreach (var transform in transforms)
        {
            transform.localPosition = anchor.localPosition + new Vector3(radius * Mathf.Cos(theta + HalfPI), anchor.localPosition.y, radius * Mathf.Sin(theta + HalfPI));
            transform.LookAt(2 * (transform.position/* - Camera.main.transform.position*/), Vector3.up);
            theta -= spacingRads;
        }
    }

    public static IPromise AnimationPromise(float duration, Action<float, Promise> iterate)
        => Promisify(Animate, duration, iterate);

    public static IPromise Promisify(Func<IEnumerator> action)
    {
        var promise = new Promise();
        App.Instance?.StartCoroutine(PromiseCoroutine(promise, action));
        return promise;
    }

    public static Func<IPromise> PromisifyCurried(Func<IEnumerator> action) => (() => Promisify(action));

    private static IEnumerator PromiseCoroutine(Promise promise, Func<IEnumerator> action)
    {
        yield return App.Instance.StartCoroutine(action());
        promise.Resolve();
    }

    // TODO: handle exceptions
    public static IPromise Promisify(
        Func<Promise, Action<float, Promise>, float, IEnumerator> action,
        float duration,
        Action<float, Promise> iterate)
    {
        var promise = new Promise();
        App.Instance?.StartCoroutine(action(promise, iterate, duration));
        return promise;
    }

    public static Func<IPromise> PromisifyCurried(
        Func<Promise, Action<float, Promise>, float, IEnumerator> action,
        float duration,
        Action<float, Promise> iterate) => (() => Promisify(action, duration, iterate));

    public static IEnumerator Animate(Promise promise, Action<float, Promise> iterate, float duration)
    {
        var tAtStart = Time.time;
        var progress = 0f;
        while (progress < 1 && promise.CurState.Equals(PromiseState.Pending))
        {
            var dT = Time.time - tAtStart;
            progress = duration.Equals(0) ? 1 : dT / duration ;
            iterate(Mathf.Min(1, progress), promise);
            yield return null;
        }
        if (promise.CurState.Equals(PromiseState.Pending)) promise.Resolve();
    }

    public static IEnumerator AnimateIndefinitely(Func<float, bool> iterate, IPendingPromise promise)
    {
        var tAtStart = Time.time;
        while (iterate(Time.time - tAtStart)) yield return null;
        promise.Resolve();
    }

    public static IPromise AnimateIndefinitely(Func<float, bool> iterate)
    {
        var promise = new Promise();
        App.Instance?.StartCoroutine(AnimateIndefinitely(iterate, promise));
        return promise;
    }
}

public class ActionInterruptedException : Exception {}

public static class TransformEx
{
    public static Transform Clear(this Transform transform)
    {
        // http://answers.unity3d.com/questions/678069/destroyimmediate-on-children-not-working-correctly.html?sort=votes
        var tempList = transform.Cast<Transform>().ToList();
        foreach (var child in tempList)
        {
            #if UNITY_EDITOR
            GameObject.DestroyImmediate(child.gameObject);
            #else
            GameObject.Destroy(child.gameObject);
            #endif
        }
        return transform;
    }

    public static Transform TransferAndDeactivateChildren(this Transform transform, Transform toTransform)
    {
        // http://answers.unity3d.com/questions/678069/destroyimmediate-on-children-not-working-correctly.html?sort=votes
        var tempList = transform.Cast<Transform>().ToList();
        foreach (var child in tempList)
        {
            child.SetParent(toTransform);
            child.gameObject.SetActive(false);
        }
        return transform;
    }
}

public static class IEnumerableExtensions
{
    public static IEnumerable<List<T>> InSetsOf<T>(this IEnumerable<T> source, int max)
    {
        List<T> toReturn = new List<T>(max);
        foreach (var item in source)
        {
            toReturn.Add(item);
            if (toReturn.Count == max)
            {
                yield return toReturn;
                toReturn = new List<T>(max);
            }
        }
        if (toReturn.Any())
        {
            yield return toReturn;
        }
    }
}