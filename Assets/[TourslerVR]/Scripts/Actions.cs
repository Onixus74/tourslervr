﻿using UnityEngine;
using UnityEngine.Events;

class Actions : MonoBehaviour
{
    private GameObject audio;
//    private OVRPlatformMenu ovrPlatformMenu;

//    private SteamVR_Controller.Device controller { get { return SteamVR_Controller.Input(1); } }
//    private SteamVR_TrackedObject trackedObj;
    const float threshold = 0.3f;

    void Start()
    {
        audio = GameObject.Find("Audio"); // this will be different depending on platform       

//        if(App.Instance.VrPlatform == Platform.VrPlatform.Vive)
//        {            
//            //trackedObj = GameObject.Find("Controller (right)").GetComponent<SteamVR_TrackedObject>();
//        }
        
    }

    void PlayAudio(string name) => audio.transform.Find(name).GetComponent<AudioSource>().Play();

    void Update()
    {
        if (Input.GetMouseButtonDown(0)) Engage();
    }

    private void Forward()
    {
        if (Tour.Instance.IsIdle)
        {
            if (Tour.Instance.TeleportForward())
                PlayAudio("Swipe");
            else
                PlayAudio("Error");
        }
        
    }

    private void Backward()
    {
        if (Tour.Instance.IsIdle)
        {
            if (Tour.Instance.TeleportBackward())
                PlayAudio("Swipe");
            else
                PlayAudio("Error");
        }
    }


    private void Left()
    {
        if (Tour.Instance.IsActive)
        {
            Tour.Instance.NextMenuPage();
        }
        else
        {
            if (TourSelectionMenu.Instance.Prev())
                PlayAudio("Swipe");
            else
                PlayAudio("Error");
        }
    }

    private void Right()
    {
        if (Tour.Instance.IsActive)
        {
            Tour.Instance.PrevMenuPage();
        }
        else
        {
            if (TourSelectionMenu.Instance.Next())
                PlayAudio("Swipe");
            else
                PlayAudio("Error");
        }
    }

    private void NoOp() { }

    public void HideUI() => NoOp();//Tour.Instance.HideUI();

    public void ShowUI() => NoOp();//Tour.Instance.ShowUI();

    public void ShowRooms() => Tour.Instance.ShowPhotoMenu();

    public void ShowFloorplans() => Tour.Instance.ShowFloorplanMenu();

    public void Exit()
    {
        if (Tour.Instance.IsActive)
        {
            App.Instance.Back();
            PlayAudio("Back");
        }
        // TODO: return to Oculus menu
        //            var activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
        //            activity.Call<bool>("moveTaskToBack", true);
    }

    public void Engage()
    {
        LinkDisc.Engage();
        SquareMenuItem.Engage();
    }

    // Tour Selection Menu Links
    public void NextMenuItem() => TourSelectionMenu.Instance.Next();
    public void PrevMenuItem() => TourSelectionMenu.Instance.Prev();
    public void EnterTour() => TourSelectionMenu.Instance.EnterTour();
}
