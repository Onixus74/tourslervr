﻿/*Mirrors the relevant parts of the config JSON pulled from the server so that we can deserialize it.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TourConfig {
    public Starting starting { get; set; }
	public List<Scene> scenes { get; set; }
	public List<Floor> floors { get; set; }
    public TourDetails tourDetails { get; set; }
    public float height { get; set; } //vertical distance between the scene center (the camera's position) and the floor
    public Photo[] photos { get; set; }

    public IEnumerable<Photo> PhotosWithCaptions
        => photos.Where(p => !String.IsNullOrEmpty(p.caption)).Take(20);

    public Scene FindScene(string sceneCode) => scenes.Find(scene => scene.code.Equals(sceneCode));
}

public class Starting
{
    public Scene scene { get; set; } // set after deserialization

    public string sceneCode { get; set; }
    public float pan { get; set; }
    public float tilt { get; set; }
}

public class Link {
	public string target { get; set; }
}

public class Floor
{
    public Texture2D texture { get; set; }

	public string name { get; set; }
	public string floorId { get; set; }
	public bool imageReady { get; set; }
	public float x { get; set; }
	public float y { get; set; }
	public float z { get; set; }
	public float scale { get; set; }
	public float cropLeft { get; set; }
	public float cropRight { get; set; }
	public float cropTop { get; set; }
	public float cropBottom { get; set; }
	public bool hidden { get; set; }

    /* JSON
 {
      "name": "Lower",
      "floorId": "jP3SBRKpYKg",
      "version": null,
      "imageReady": true,
      "x": -82.433900368140485,
      "y": 1047.2504792558798,
      "z": 0,
      "scale": 1.6933333333333334,
      "cropLeft": 0,
      "cropRight": 0,
      "cropTop": 0,
      "cropBottom": 0,
      "boundaries": null,
      "rooms": null,
      "hidden": true
    },*/
}

public class Photo
{
    public Texture2D texture { get; set; }
    public Scene sceneObj { get; set; }
    public string scene { get; set; }
    public float yaw { get; set; }
    public float absoluteYaw { get; set; }
    public float pitch { get; set; }
    public float x { get; set; }
    public float y { get; set; }
    public string floor { get; set; }
    public string caption { get; set; }
    public string filename { get; set; }
    public string externalLink { get; set; }
    public bool stillOnly { get; set; }
}
/*
 {
      "filename": "nU9xdpK9tWQ",
      "scene": "fxaFKMLoNMN",
      "yaw": -137.865849833486,
      "pitch": 0,
      "caption": "Exterior",
      "version": -294713721,
      "externalLink": null,
      "x": -66.14030965105087,
      "y": -1905.4152372411361,
      "floor": "2HuQJo34wdL",
      "absoluteYaw": -92.623011659430887,
      "stillOnly": false
    },*/
