﻿using System;
using System.Collections;
using RSG;
using UnityEngine;
using static Util;

public class Panorama : MonoBehaviour
{
    private Material material;

    void Awake()
    {
        material = gameObject.GetComponent<Renderer>().material;
    }

    public float Diameter => gameObject.transform.localScale.x;

    public void SetScene(Scene scene)
    {
        SetCubeMap(scene.cubemap);
        Rotate(scene.yaw);
    }

    private void SetCubeMap(Cubemap cubemap)
    {
        material.SetTexture("_Tex", cubemap);
    }

    private void Rotate(float yaw)
    {
        gameObject.transform.rotation = Quaternion.identity;
        gameObject.transform.Rotate(0, -yaw + 90, 0);
    }

    public IPromise TransitionTo(Vector3 startPos, Vector3 endPos, float duration, float finalAlpha = 1)
    {
        var initialAlpha = Alpha;
        return AnimationPromise(duration, (progress, promise) =>
        {
            SetMaterialAlpha(Mathf.Lerp(initialAlpha, finalAlpha, Mathf.Pow(progress, 3)));
            gameObject.transform.position = Vector3.Lerp(startPos, endPos, progress);
        });
    }


    public IPromise TransitionFrom(Vector3 startPos, Vector3 endPos, float duration)
    {
        var initialAlpha = Alpha;
        return AnimationPromise(duration, (progress, promise) =>
        {
            SetMaterialAlpha(Mathf.Lerp(initialAlpha, 0, progress * 2));
            gameObject.transform.position = Vector3.Lerp(startPos, endPos, progress);
        });
    }
        

    public void SetMaterialAlpha(float alpha)
    {
        material.SetFloat("_Alpha", alpha);
    }

    public float Alpha => material.GetFloat("_Alpha");

    public IPromise FadeOut(float seconds) => FadeOut(seconds, 0);

    public void FadeOut(float seconds, Action then) => FadeOut(seconds, 0, then);

    public IPromise FadeOut(float seconds, float finalAlpha, Action then = null)
    {
        var alpha = Alpha;
        var promise = new Promise();
        StartCoroutine(LerpAlpha(alpha, Mathf.Min(alpha, finalAlpha), seconds, () =>
        {
            promise.Resolve();
            then?.Invoke();
        }));
        return promise;
    }

    public void FadeIn(float seconds, float finalAlpha, Action then = null)
    {
        StartCoroutine(LerpAlpha(0, finalAlpha, seconds, then));
    }

    private IEnumerator LerpAlpha(float start, float end, float seconds, Action then = null)
    {
        var t = Time.time;
        while (Time.time - t < seconds * 1.1f)
        {
            var alpha = Mathf.Lerp(start, end, (Time.time - t)/seconds);
            SetMaterialAlpha(alpha);
            yield return null;
        }
        then?.Invoke();
    }
}