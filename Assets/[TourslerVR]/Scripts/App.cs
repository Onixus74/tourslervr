﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Linq;
using RSG;
using UnityEngine.UI;
using static Util;
using Application = UnityEngine.Application;

public class App : Singleton<App> {
    protected App () { }

//    private Platform platform;
//    public Platform.VrPlatform VrPlatform { get { return platform.CurrentPlatform; } }

    // "Constants" made public to tweak in editor
    public float PanoramaFadeInSeconds = 0.3f;
    public float PanoramaFadeOutSeconds = 0.3f;
    public float ReturnToMenuFadeSeconds = 1f;
    public float SwipeRotationMultiplier = -0.4f;
    public bool IsOfflineMode;
    public float LinkCollisionRadiusExponent = 0.1f;


    private GameObject cardboardHeadRotationTarget;
    private GameObject loadingCoin;

    private List<Panorama> panoramas = new List<Panorama>(2);
    private int currPanoramaIdx;
    private int nextPanoramaIdx => (currPanoramaIdx + 1) % 2;
    private Panorama CurrPanorama => panoramas[currPanoramaIdx];
    private Panorama NextPanorama => panoramas[nextPanoramaIdx];

    private void SwapPanoramas()
    {
        currPanoramaIdx = nextPanoramaIdx;
        panoramas.ForEach(p => p.transform.position = Vector3.zero);
    }

    public void HidePanoramasImmediately() => panoramas.ForEach(p => p.SetMaterialAlpha(0));

    public void DimPanoramas() => panoramas.ForEach(p => p.FadeOut(PanoramaFadeOutSeconds, 0.03f));

    public IPromise HidePanoramas() => Promise.All(panoramas.Select(p => p.FadeOut(PanoramaFadeOutSeconds)));

    public void ShowCurrentPanorama() => CurrPanorama.FadeIn(PanoramaFadeInSeconds, 1);

    public IPromise TransitionPanoramas(Scene scene, float duration, float finalAlpha = 1) =>
        TransitionPanoramas(null, scene, duration, finalAlpha);

    public IPromise TransitionPanoramas(Scene fromScene, Scene toScene, float duration, float finalAlpha = 1)
    {
        NextPanorama.SetScene(toScene);

        var translation = fromScene == null ? Vector3.zero : toScene.position - fromScene.position;
        var mag = (float)Math.Pow((1 - Math.Exp(-translation.magnitude / 200)), 2); // map magnitude (0 to infinity) to a curve with values (0 to 1)
        var panoDiameter = NextPanorama.Diameter; // W is the side length of the target cube
        var constrainedTranslation = translation.normalized * mag * 0.4f * panoDiameter;

        return Promise.All(
            NextPanorama.TransitionTo(constrainedTranslation, Vector3.zero, duration, finalAlpha),
            CurrPanorama.TransitionFrom(Vector3.zero, -constrainedTranslation, duration))
            .Then(() => SwapPanoramas());
    }

    private GameObject splash;
    private Text splashInfo;
    public void AppendSplashMessage(string msg) => splashInfo.text = splashInfo.text + "\n" + msg;

    public void SetSplashMessage(string msg)
    {
        splashInfo.text = msg;
        splash.SetActive(true);
    } 
    public void HideSplash()
    {
        HideLoadingCoin();
        splash.SetActive(false);
    }

    public void SetLoadingCoinPosition(Vector3 position)
    {
        loadingCoin.transform.rotation = Quaternion.identity;
        loadingCoin.transform.position = position;
        loadingCoin.SetActive(true);
    }

    public void HideLoadingCoin() => loadingCoin.SetActive(false);

    void Start()
    {
//        platform = GameObject.Find("VrPlatform").GetComponent<Platform>();

        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        cardboardHeadRotationTarget = GameObject.Find("HeadRotationTarget");
        loadingCoin = GameObject.Find("LoadingCoin");

        panoramas = new[] { "PhotoCubeA", "PhotoCubeB" }.Select(s => GameObject.Find(s).GetComponent<Panorama>()).ToList();
        panoramas.ForEach(p => p.transform.position = Vector3.zero);
        HidePanoramasImmediately();

        splash = GameObject.Find("Splash");
        splashInfo = splash.transform.Find("Info").GetComponentInChildren<Text>();

        SetupExceptionLogging();

        Tour.Instance.Init();

        TourSelectionMenu.Instance.Show();

    }

    // LaunchFromUrl() gets called by TourlserAndroidPlugin's TourslerVrActivity when app launches from URL
    // TourslerVrActivity expects this script to be attached to a GameObject called "App" 
    public void LaunchFromUrl(string url)
    {
        Debug.Log("Loading Tour... " + url);
        var tourName = url.Split('/').Last(); // http://toursler.com/vr/<tourid>
        SetSplashMessage("Loading Tour..." + tourName);

        TourSelectionConfig tourSelectionConfig = new TourSelectionConfig(tourName);
        TourSelectionMenu.Instance.tourSelections.Clear();
        TourSelectionMenu.Instance.Hide();
        TourSelectionMenu.Instance.tourSelections_tmp.Add(tourSelectionConfig);
        TourSelectionMenu.Instance.tourSelections.Add(tourSelectionConfig);

        TourSelectionMenu.Instance.launchedFromBrowser = true;

        TourSelectionMenu.Instance.SetupTourSelection(tourSelectionConfig)

        .Then(() => TourSelectionMenu.Instance.AppendTourSelection(tourName, true))
        .Then(() =>
            {
                EnterTour(tourName);
                HidePanoramas();
                Debug.Log("entred");
            });
        // instead of entering the tour directly it might be easier to add this item to the main menu as it loads
    }

    private void EnterTour(string tourName)
    {
        TourSelectionMenu.Instance.Hide();
        HidePanoramasImmediately();
        Tour.Instance.Load(TourSelectionMenu.Instance.GetTourConfig(tourName))
            .Then(() => HideSplash());
    }

    public void EnterTour(TourConfig tourConfig)
    {
        Promise.All(TourSelectionMenu.Instance.Hide(), HidePanoramas())
            .Then(() => Tour.Instance.Load(tourConfig));
    }

    public void Back()
    {
        if (Tour.Instance.WillExitOnBack) ReturnToMenu(); else Tour.Instance.Back();
    }

    private void ReturnToMenu() => Tour.Instance.Exit()
        .Then(() => CurrPanorama.FadeOut(ReturnToMenuFadeSeconds))
        .Then(() => TourSelectionMenu.Instance.Show());

    void Update()
    {
        // two-finger swipe to rotate view
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            cardboardHeadRotationTarget.transform.Rotate(0, SwipeRotationMultiplier*Input.GetTouch(0).deltaPosition.x, 0);
        if (Input.GetKeyDown(KeyCode.Space))
        {
            LaunchFromUrl("/115-craven-ave");
        }

        if(loadingCoin.activeSelf) loadingCoin.transform.Rotate(Vector3.up, 360f * Time.deltaTime);
    }

    //    void LateUpdate() {
    //    Cardboard.SDK.UpdateState();
    //    if (Cardboard.SDK.BackButtonPressed) {
    //      Application.Quit();
    //    }
    //  }

    private StreamWriter streamWriter;

    private void HandleLogMessage(string condition, string stackTrace, LogType type)
        => LogMessage(condition, stackTrace, type);

    private void LogMessage(string condition, string stackTrace, LogType type, Exception e = null)
    {
        var logType = type.Equals(LogType.Exception) ? type.ToString().ToUpper() : type.ToString();
        var logString = $"{logType}: condition:{condition}\n{stackTrace}\nsource: {e?.Source}\nhelpLink: {e?.HelpLink}";
        streamWriter.WriteLine(logString);
        if(type.Equals(LogType.Exception) && !e.GetType().Equals(typeof(ActionInterruptedException))) SetSplashMessage("Error Loading. Please exit and try again.");
        Log(logString);
		Debug.LogError ("[ERROR]"+logString);
    }

    private void HandlePromiseException(object sender, ExceptionEventArgs e) =>
        LogMessage("message: " + e.Exception.Message, e.Exception.StackTrace, LogType.Exception, e.Exception);

    private void SetupExceptionLogging()
    {
        Application.logMessageReceived += HandleLogMessage;
        Promise.UnhandledException += HandlePromiseException;
        streamWriter = new StreamWriter(Application.persistentDataPath + Path.DirectorySeparatorChar + "_debuglog.txt");
        streamWriter.AutoFlush = true;
    }
}
