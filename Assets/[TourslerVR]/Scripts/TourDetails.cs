﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using UnityEngine;

public class TourDetails
{
    public string title { get; set; }
    public string address { get; set; }
    public string company { get; set; }
    public float? price { get; set; } // in USD
    public int? areaPrimary { get; set; } // in sq. ft.
    public int? areaSecondary { get; set; }
    public int? parkingPrimary { get; set; }
    public int? parkingSecondary { get; set; }
    public int? bedroomsPrimary { get; set; }
    public int? bedroomsSecondary { get; set; }
    public int? bathroomsPrimary { get; set; }
    public int? bathroomsSecondary { get; set; }

    public Contact[] contacts { get; set; }
    public DetailsPhoto[] photos { get; set; }
}

public class Contact
{
    public string name { get; set; }
    public string title { get; set; }
    public string company { get; set; }
    public string email { get; set; }
    public string website { get; set; }
    public string phoneDirect { get; set; }
    public string phoneOffice { get; set; }
    public string photoUrl { get; set; }
    public string bannerUrl { get; set; }

}

public class DetailsPhoto
{
    public string caption { get; set; }
    public string filename { get; set; }
    public string externalLink { get; set; }
    public string urlTiny { get; set; }
    public string urlSmall { get; set; }
    public string urlMedium { get; set; }
    public string urlFull { get; set; }
}

/*{
  "id": "fXK4raQXzoU",
  "name": "102-knox",
  "title": "Leslieville Three Bedroom",
  "price": null,
  "address": "102 Knox Avenue\nToronto, ON M4L 2P2",
  "areaPrimary": 1191,
  "areaSecondary": 573,
  "parkingPrimary": null,
  "parkingSecondary": null,
  "bedroomsPrimary": 3,
  "bedroomsSecondary": null,
  "bathroomsPrimary": 2,
  "bathroomsSecondary": null,
  "mlsId": null,
  "mslUrl": null,
  "company": "Toursler",
  "contacts": [
    {
      "id": "tFit87KvgbA",
      "name": "Cameron Weir & Scott Hanton",
      "title": "The Weir Team",
      "company": "Keller Williams Advantage Realty",
      "email": "listings@theweirteam.ca",
      "website": "TheWeirTeam.ca",
      "phoneDirect": "416-465-4545",
      "phoneOffice": null,
      "photoUrl": null,
      "bannerUrl": null
    }
  ],
  "photos": [
    {
      "id": "nU9xdpK9tWQ",
      "tourName": "102-knox",
      "caption": "Exterior",
      "filename": "0012.jpg",
      "externalLink": null,
      "urlTiny": "https://d20gdid2ajppdc.cloudfront.net/nU9xdpK9tWQ_tiny.jpg",
      "urlSmall": "https://d20gdid2ajppdc.cloudfront.net/nU9xdpK9tWQ_small.jpg",
      "urlMedium": "https://d20gdid2ajppdc.cloudfront.net/nU9xdpK9tWQ_medium.jpg",
      "urlFull": "https://d20gdid2ajppdc.cloudfront.net/nU9xdpK9tWQ_full.jpg"
    },   
  ]
}*/
