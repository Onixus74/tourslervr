﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using RSG;

class Tour : Singleton<Tour>, IStateTransitions
{
    protected Tour() { }

    private TourConfig tourConfig;
    private Scene currentScene;
    private readonly List<Scene> sceneHistory = new List<Scene>();
    private readonly List<Scene> downloadHistory = new List<Scene>();

    // State Transitions
    private States state;
    public Enum State => state;
    public IPromise CurrentTransition { get; set; }
    //TODO: figure out how to hide these from this class. Want abstract impl but no multi-inheritance. Static "state transitions system" Dictionary<Component, State<IsDisabled, CurrentTransitions>> where CurrentTransitions derived from Promise, adding Cancelled to Promise state
    public bool IsDisabled { get; set; }
    public bool IsCurrentTransitionCancelled { get; set; }

    public bool IsIdle => state.Equals(States.Idle);
    public bool IsActive => !state.Equals(States.Inactive);

    public const int FadeInSeconds = 3;
    public const float Scale = 0.06f; // 1 scene unit = 1cm = 0.01m = 1 Unity unit TODO: fudge factor for now since this doesn't really work well with our panoramas. figure out how to change FOV for them
    public const float TransitionSeconds = 1f;
    public const int InclusionRadius = 25;

    private GameObject cameraObj;
    private GameObject redLobsterMenu;
    private PhotoMenu photoMenu;
    private FloorplanMenu floorplanMenu;

    private readonly Dictionary<Scene, GameObject> links = new Dictionary<Scene, GameObject>();
    private Scene closestSceneByAngularDistance;
    private Scene farthestSceneByAngularDistance;

    private static Vector3 GetScenePosition(Scene scene) => new Vector3(scene.x, scene.z, scene.y) * Scale;

    private bool didInit;
    public void Init()
    {
        if (didInit) return; // if redLobsterMenu has been set to inactive, we won't be able to find it again so only init once
        didInit = true;
        cameraObj = Camera.main.gameObject;
        redLobsterMenu = GameObject.Find("RedLobsterMenu");
        photoMenu = GameObject.Find("PhotoMenu").GetComponent<PhotoMenu>();
        floorplanMenu = GameObject.Find("FloorplanMenu").GetComponent<FloorplanMenu>();
        DeactivateRedLobsterMenu();
    }

    public IPromise Load(string tourName) => AssetLoader.DownloadTourConfig(tourName).Then(Load);

    public IPromise Load(TourConfig tourConfig)
    {
        this.tourConfig = tourConfig;
        GameObject.Find("PanoramaContainer").transform.rotation = Quaternion.Euler(0, tourConfig.starting.pan - 90, 0);
        tourConfig.scenes.ForEach(SetupLinksForScene);
        currentScene = tourConfig.FindScene(tourConfig.starting.sceneCode);
        sceneHistory.Add(currentScene);

        var photosPromise = Promise.All(tourConfig.PhotosWithCaptions.Select(photo =>
        {
            photo.sceneObj = tourConfig.FindScene(photo.scene);
            return AssetLoader.DownloadPhotoTexture(photo);
        }));

        var promise = Promise.All(photosPromise, LoadSceneTextures(currentScene))
            .Then(() => App.Instance.TransitionPanoramas(currentScene, FadeInSeconds))
            .Then(() =>
            {
                ActivateRedLobsterMenu();
                ActivateSceneLinks(currentScene);
                SetState(States.Idle);
            });

        return SetState(States.Loading, promise);
    }

    private void SetupLinksForScene(Scene scene)
    {
        // transform list of SceneIds to list of scene object refs
        scene.scenes = scene.linkedScenes.Select(tourConfig.FindScene);

        var link = (GameObject)Instantiate(Resources.Load("Link"), GetScenePosition(scene), Quaternion.identity);
        link.SetActive(false);

        var linkDisc = link.GetComponent<LinkDisc>();
        linkDisc.OnEngage.AddListener(() => TeleportToScene(scene, true));
        link.transform.localScale = Vector3.one * 1.5f;
        links.Add(scene, link);
    }

    public bool WillExitOnBack => !state.Equals(States.Loading) && !state.Equals(States.ModalMenu);

    public void Back()
    {
        HidePhotoMenu();
        HideFloorplanMenu();
    }

    public IPromise Exit() => SetState(States.Inactive);

    private void CalculateForwardAndBackwardScenes()
    {
        var currScenePos = GetScenePosition(currentScene);
        var minAngle = 45f;
        var maxAngle = 45f;
        closestSceneByAngularDistance = null;
        farthestSceneByAngularDistance = null;
        foreach (Scene linkedScene in currentScene.scenes)
        {
            var dScenePos = GetScenePosition(linkedScene) - currScenePos;
            var angle = Vector3.Angle(dScenePos, cameraObj.transform.forward);
            if (angle < minAngle)
            {
                closestSceneByAngularDistance = linkedScene;
                minAngle = angle;
            }

            if (angle > maxAngle)
            {
                farthestSceneByAngularDistance = linkedScene;
                maxAngle = angle;
            }
        }
    }

    public bool TeleportToScene(Scene scene, bool showLoadingCoin = false)
    {
        // no triggering another transition while one is in progress
        if (state != States.Idle || tourConfig == null || scene == null)
        {
            return false;
        }
        else
        {
            DeactivateSceneLinks();
            sceneHistory.Insert(0, scene);
            ReleaseResources();

            if (showLoadingCoin)
            {
                var diff = GetScenePosition(scene) - GetScenePosition(currentScene);
                App.Instance.SetLoadingCoinPosition(diff + (Vector3.down * 3));
            }

            var promise = LoadSceneTextures(scene)
                .Then(() =>
                {
                    App.Instance.HideLoadingCoin();
                    return App.Instance.TransitionPanoramas(currentScene, scene, TransitionSeconds);
                })
                .Then(() =>
                {
                    if (isUIVisible) ActivateSceneLinks(scene);
                    ActivateRedLobsterMenu();
                    currentScene = scene;
                    SetState(States.Idle);
                });

            SetState(States.Transitioning, promise);
            
            return true;
        }
    }

    public void PrevMenuPage() => photoMenu.PrevPage();

    public void NextMenuPage() => photoMenu.NextPage();

    public bool TeleportForward() => TeleportToScene(closestSceneByAngularDistance);

    public bool TeleportBackward() => TeleportToScene(farthestSceneByAngularDistance);

    int numScenesToCache = 10;
    private void ReleaseResources()
    {   // release resources from scenes we haven't visited in a while as well as downloaded scenes we've never visited
        var distinctSceneHistory = sceneHistory.Distinct();
        // TODO: downloadHistory can be removed when storage cache is implemented
        var distinctDownloadHistory = downloadHistory.Except(distinctSceneHistory).Distinct();
        var immuneFromRemoval = distinctSceneHistory.Take(numScenesToCache)
            .Concat(distinctDownloadHistory.Take(numScenesToCache));

        Util.Log("Safe Scenes:", string.Join(", ", immuneFromRemoval.Select(scene => scene.code).ToArray()));

        tourConfig.scenes.Except(immuneFromRemoval).ToList().ForEach(scene => scene.ClearTextures());
    }

    private void ClearAllSceneTextures() => tourConfig.scenes.Where(s => !s.Equals(tourConfig.starting.scene)).ToList().ForEach(scene => scene.ClearTextures());

    private IPromise LoadSceneTextures(Scene scene)
    {
        var scenesToDownload = scene.scenes.Where(s => !s.texturesQueuedForDownload);
        downloadHistory.InsertRange(0, scenesToDownload);

        // if (!App.Instance.IsOfflineMode)
        //     scene.scenes.Where(s => !s.texturesQueuedForDownload).ToList().ForEach(AssetLoader.EnqueueLoadSceneTextures);

        return AssetLoader.LoadSceneTextures(scene);
    }

    private IEnumerable<KeyValuePair<Scene, GameObject>> GetVisibleSceneLinks(Scene scene)
    {
        return links.Where(pair =>
        {
            var diff = GetScenePosition(pair.Key) - GetScenePosition(scene);
            var isInBounds = diff.magnitude < InclusionRadius;
            var isLinked = scene.linkedScenes.Contains(pair.Key.code);
            return !scene.Equals(pair.Key) && (isInBounds && !isLinked) || (!isInBounds && isLinked);
        });
    }

    private void ActivateSceneLinks(Scene scene)
    {
        foreach (var pair in GetVisibleSceneLinks(scene))
        {
            var linkScene = pair.Key;
            var link = pair.Value;
            var diff = GetScenePosition(linkScene) - GetScenePosition(scene);
            link.transform.position = diff + (Vector3.down * 3);
            link.transform.rotation = Quaternion.LookRotation(link.transform.position - cameraObj.transform.position);

            link.name = $"Link-{linkScene.code}";
            var r = Mathf.Pow(diff.magnitude, App.Instance.LinkCollisionRadiusExponent);
            link.transform.GetComponentInChildren<SphereCollider>().radius = r;
            link.GetComponent<LinkDisc>().Init();
        }
    }

    private void DeactivateSceneLinks() => links.Values.ToList().ForEach(link => link.SetActive(false));

    private bool isUIVisible = true;
    public void ShowUI()
    {
        if (!isUIVisible && state == States.Idle)
        {
            isUIVisible = true;
            ActivateSceneLinks(currentScene);
        }
    }

    public void HideUI()
    {
        if (IsActive)
        {
            isUIVisible = false;
            DeactivateSceneLinks();
        }
    }

    void BeforeHideMenu(bool skipShowPano = false)
    {
        if (!skipShowPano)
        {
            App.Instance.ShowCurrentPanorama();
        }
        SetState(States.Idle);
        ShowUI();
    }

    public void ShowPhotoMenu()
    {
        SetState(States.ModalMenu);
        photoMenu.Show(tourConfig.PhotosWithCaptions.Where(p => !currentScene.code.Equals(p.scene)));
    }

    public void ShowFloorplanMenu()
    {
        SetState(States.ModalMenu);
        floorplanMenu.Show(tourConfig.floors, tourConfig.photos, currentScene);
    }

    public void HidePhotoMenu(bool skipShowPano = false)
    {
        BeforeHideMenu(skipShowPano);
        photoMenu.Hide();
    }

    public void HideFloorplanMenu(bool skipShowPano = false)
    {
        BeforeHideMenu(skipShowPano);
        floorplanMenu.Hide();
    }

    public void ActivateRedLobsterMenu() => redLobsterMenu.SetActive(true);

    public void DeactivateRedLobsterMenu() => redLobsterMenu.SetActive(false);

    void Unload()
    {
        links.Values.ToList().ForEach(Destroy);
        links.Clear();

        ClearAllSceneTextures(); // TODO: move to after transition
        sceneHistory.Clear();
        downloadHistory.Clear();
        currentScene = null;
    }

    IPromise SetState(States nextState, IPromise transition = null)
    {
        state = nextState;

        switch (nextState)
        {
            case States.Inactive:
                DeactivateRedLobsterMenu();
                if (null != CurrentTransition && ((Promise)CurrentTransition).CurState.Equals(PromiseState.Pending)) // TODO: add to Util
                    CurrentTransition.Then(() => Unload()); 
                else Unload();

                break;
            case States.Loading:
                break;
            case States.Idle:
                ActivateRedLobsterMenu();
                this.SetCurrentIdleAnimation(dT =>
                {
                    if (state.Equals(States.Idle) && currentScene != null) CalculateForwardAndBackwardScenes();
                    return state.Equals(States.Idle);
                });
                break;
            case States.Transitioning:
                DeactivateRedLobsterMenu();
                CurrentTransition = transition;
                break;
            case States.ModalMenu:
                App.Instance.DimPanoramas();
                DeactivateRedLobsterMenu();
                HideUI();
                break;
        }

        return CurrentTransition??Promise.Resolved();
    }

    enum States
    {
        Inactive,
        Loading,
        Idle,
        Transitioning,
        ModalMenu,
    }
}