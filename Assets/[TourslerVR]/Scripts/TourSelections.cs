﻿using UnityEngine;
using System.Collections.Generic;

public class TourSelections : List<TourSelectionConfig>
{
    public TourSelections(IEnumerable<TourSelectionConfig> selections) : base(selections){ }
}

public class TourSelectionConfig
{
    public TourSelectionConfig()
    {
    }

    public TourSelectionConfig(string tourName)
    {
        this.tourName = tourName;
        isFeatured = false;
    }

    public TourConfig tourConfig { get; set; }
    public TourDetails tourDetails { get; set; }
    public bool isFeatured = true;

    // from JSON
    public string tourName { get; set; } // this is the only thing we really use from TourSelection JSON
    public string title { get; set; }
    public string subtitle { get; set; }
    public string description { get; set; }
    public string url { get; set; }
    public string thumbnail { get; set; }

    public override bool Equals(object obj) => obj == null ? false : (obj as TourSelectionConfig).tourName == tourName;

    public override int GetHashCode() => tourName.GetHashCode();
}