﻿using System;
using System.IO;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RSG;
using UnityEngine.UI;
using static Util;

class TourSelectionMenu : Singleton<TourSelectionMenu>
{
    protected TourSelectionMenu() { }
    private bool isLoading;
    private bool isLoaded;

    public List<TourSelectionConfig> tourSelections = new List<TourSelectionConfig>();
    public List<TourSelectionConfig> tourSelections_tmp = new List<TourSelectionConfig>();
    private List<string> recentlyVisited = new List<string>();
    public TourConfig GetTourConfig(string tourName) => tourSelections.First(s => s.tourName.Equals(tourName)).tourConfig;
    private int currSelectionIdx = 0;
    public int currentIdx = 0;
    private TourSelectionConfig currentSelection => tourSelections[currSelectionIdx];
    private Scene currentStartingScene => currentSelection.tourConfig.starting.scene;
    private ListingDetails listingDetails;

    private GameObject panoramaContainer;
    private GameObject menuGameObject;

    private Transform linkContainer;

    private const bool downloadEverything = false;
    private Floorplans floorplans;

    void Awake()
    {
        listingDetails = GameObject.Find("ListingDetails").GetComponent<ListingDetails>();
        menuGameObject = GameObject.Find("TourSelectionMenu");
        linkContainer = GameObject.Find("LinkContainer").transform;
        floorplans = GameObject.Find("FloorplanContainer").GetComponent<Floorplans>();
        panoramaContainer = GameObject.Find("PanoramaContainer");
        menuGameObject.SetActive(false);
        AssetLoader.DownloadTourList()
            .Then(ts =>
            {
                this.tourSelections_tmp.AddRange(ts);
                LaunchTour("/" + tourSelections_tmp[0].tourName);
            });
    }


    public void Show()
    {
        // if (isLoaded)
        // {
        //     Activate();
        // }
        // else
        // {
        //     if (!isLoading)
        //     {
                // isLoading = true;
                Load().Then(() =>
                {
                    isLoaded = true;
                    isLoading = false;
                    Activate();
                    // if (!launchedFromBrowser) Activate(); // Tour may be loading or in progress 
                });
            // }
        // }
    }

    public bool launchedFromBrowser = false;
    public IPromise AppendTourSelection(string tourName, bool launchedFromBrowser = false)
    {
        if (launchedFromBrowser) this.launchedFromBrowser = true;
        var tourSelection = new TourSelectionConfig(tourName);
        if (!tourSelections.Contains(tourSelection))
        {
            tourSelections.Add(tourSelection);
            if (launchedFromBrowser) currSelectionIdx = tourSelections.IndexOf(tourSelection);
            return SetupTourSelection(tourSelection).Then(() =>
            {
                recentlyVisited.Add(tourName);
                AssetLoader.SaveRecentlyVisited(recentlyVisited);
            });
        }
        else return Promise.Resolved();
    }

    private IPromise Load()
    {
        var msg = App.Instance.IsOfflineMode ?
            downloadEverything ? "Downloading Everything... \n Please Wait." : "Loading from " + Application.persistentDataPath
            : "Loading...";

        if (!App.Instance.IsOfflineMode)
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
                msg = "Realvision VR requires a reliable internet connection. Please check your connection.";
            else if (Application.internetReachability != NetworkReachability.ReachableViaLocalAreaNetwork)
                msg = "Slowly Loading... Realvision VR requires a reliable internet connection. Please connect to WiFi.";
        }

        App.Instance.SetSplashMessage(msg);
        tourSelections.Add(tourSelections_tmp.FirstOrDefault());
        
        // return AssetLoader.DownloadTourList()
        //     .Then(tourSelections =>
        //     {
        //         this.tourSelections.Add(tourSelections.First());
        //     })
        //     .ThenAll(tourSelections => tourSelections.Select(SetupTourSelection))
        //     .Then(() => AssetLoader.LoadRecentlyVisited()) // TODO this could be a parallel operation
        //     .ThenAll(tourNames => tourNames.Select(tn => AppendTourSelection(tn)))
        //     .Then(MaybeDownloadAllTexturesForTours);
        return Promise.Resolved();
    }
    public void LaunchTour(string url)
    {
        Debug.Log("Loading Tour... " + url);
        var tourName = url.Split('/').Last(); // http://toursler.com/vr/<tourid>
        var currentTour = tourSelections_tmp.Find(x => x.tourName == tourName);
        currentIdx = tourSelections_tmp.IndexOf(currentTour);
        // var prevTour = tourSelections_tmp[currentIdx - 1];
        // var nextTour = tourSelections_tmp[currentIdx + 1];

        // if (prevTour == null){
        //     prevTour = tourSelections_tmp[tourSelections_tmp.Count()-1];
        // }

        // if (nextTour == null){
        //     nextTour = tourSelections_tmp[0];
        // }

        App.Instance.SetSplashMessage("Loading Tour...");
        // isLoading=false;
        // isLoaded = false;
        // isLoading = true;
        App.Instance.DimPanoramas();
        HideImmediately();

        // tourSelections.ForEach(x=>x.tourConfig.floors.ForEach(y=> Destroy(y.texture)));

        tourSelections.Clear();
        // tourSelections.Add(prevTour);
        tourSelections.Add(currentTour);
        // tourSelections.Add(nextTour);
        Resources.UnloadUnusedAssets();

        SetupTourSelection(currentTour)
        .Then(() =>
        {
            if (!launchedFromBrowser)
            {
                UpdateSelection();
            }
            AppendTourSelection(tourName, true);
        })
        .Then(() => App.Instance.HideSplash())
        .Then(() => AlignMenuToCamera());
    }
    public IPromise SetupTourSelection(TourSelectionConfig tourSelection)
    {
        var tourCfgPromise = AssetLoader.DownloadTourConfig(tourSelection.tourName)
                    .Then(tourConfig => SetupTourConfig(tourConfig, tourSelection));

        var tourDetailsPromise = AssetLoader.DownloadTourDetails(tourSelection.tourName)
            .Then(tourDetails =>
            {
                tourSelection.tourDetails = tourDetails;
                return Promise.Resolved();
            });

        return Promise.All(tourCfgPromise, tourDetailsPromise);
    }

    private void AlignMenuToCamera() => menuGameObject.transform.rotation = Quaternion.LookRotation(Vector3.ProjectOnPlane(Camera.main.transform.forward, Vector3.up), Vector3.up);

    private void Activate()
    {
        linkContainer.Cast<Transform>().ToList().ForEach(link => link.GetComponent<LinkDisc>().Init());
        UpdateSelection();
        App.Instance.HideSplash();
    }

    private IPromise SetupTourConfig(TourConfig tourConfig, TourSelectionConfig tourSelection)
    {
        tourSelection.tourConfig = tourConfig;
        tourConfig.floors = tourConfig.floors.Where((floor) => !floor.hidden).ToList();
        tourConfig.starting.scene = tourConfig.FindScene(tourConfig.starting.sceneCode);
        var floorsPromise = Promise.All(tourConfig.floors.Select(AssetLoader.DownloadFloorplanTexture));

        if (tourSelection.Equals(currentSelection) && !launchedFromBrowser /*LaunchTour gets called some time after initial load of menu.*/)
        {
            return Promise.All(floorsPromise, AssetLoader.LoadSceneTextures(tourConfig.starting.scene));
        }
        else
        {
            // AssetLoader.EnqueueLoadSceneTextures(tourConfig.starting.scene);
            // return floorsPromise;
            return Promise.Resolved();
        }
    }

    public IPromise MaybeDownloadAllTexturesForTours()
    {
        if (!downloadEverything) return Promise.Resolved();

        var promises = tourSelections
            .Select(selection => selection.tourConfig)
            .Select(tourConfig => Promise.All(
                    Promise.All(tourConfig.scenes.Select(AssetLoader.DownloadTexturesForScene)),
                    Promise.All(tourConfig.photos.Select(AssetLoader.DownloadPhotoTexture))
                    ));
        return Promise.All(promises);
    }

    private IPromise currTransition;
    private bool IsTransitioning => (currTransition as Promise)?.CurState.Equals(PromiseState.Pending) ?? false;

    private void ShowCurrentPanorama(float duration = 2)
    {
        if (!IsTransitioning)
        {
            currTransition = App.Instance.HidePanoramas()
                .Then(
                    () =>
                        panoramaContainer.transform.rotation =
                            Quaternion.Euler(0, currentSelection.tourConfig.starting.pan - 90, 0))
                .Then(() => App.Instance.TransitionPanoramas(currentSelection.tourConfig.starting.scene, duration, 0.5f))
                .Then(()=> menuGameObject.SetActive(true));
        }
    }

    private void UpdateListingDetails()
    {
        var details = currentSelection.tourDetails;
        var addressLines = details.address.Split('\n');
        var contact = details.contacts.Length == 0 ? null : details.contacts.First();
        listingDetails.Broker = contact?.company;
        listingDetails.Address = addressLines.First();
        listingDetails.City = addressLines.Length > 1 ? addressLines[1] : "";

        listingDetails.Data = new List<Datum>();
        if (details.areaPrimary.HasValue)
            listingDetails.Data.Add(new Datum("SQUARE FEET", new[] { details.areaPrimary, details.areaSecondary }.Sum()));
        if (details.bedroomsPrimary.HasValue)
            listingDetails.Data.Add(new Datum("BEDROOMS", FormatPriSec(details.bedroomsPrimary, details.bedroomsSecondary)));
        if (details.bathroomsPrimary.HasValue)
            listingDetails.Data.Add(new Datum("BATHROOMS", FormatPriSec(details.bathroomsPrimary, details.bathroomsSecondary)));
        if (details.parkingPrimary.HasValue)
            listingDetails.Data.Add(new Datum("PARKING", FormatPriSec(details.parkingPrimary, details.parkingSecondary)));

        listingDetails.IsFeatured = currentSelection.isFeatured;

        floorplans.Load(currentSelection.tourConfig.floors);
    }

    private static string FormatPriSec(int? primary, int? secondary) => $"{primary}{secondary?.ToString().Insert(0, "+")}";

    public bool Prev()
    {
        
        if (menuGameObject.activeSelf && !IsTransitioning)
        {
            launchedFromBrowser = false;
            LaunchTour("/" + tourSelections_tmp[(currentIdx + tourSelections_tmp.Count - 1) % tourSelections_tmp.Count].tourName);
            // UpdateSelection();
            return true;
        }
        return false;
    }

    public bool Next()
    {        
        if (menuGameObject.activeSelf && !IsTransitioning)
        {
            launchedFromBrowser = false;
            LaunchTour("/" + tourSelections_tmp[(currentIdx + 1) % tourSelections_tmp.Count].tourName);
            
            // Debug.Log("[Current] " + currentIdx);
            // Debug.Log("[Current2] " + currSelectionIdx);
            // UpdateSelection();
            return true;
        }
        return false;
    }

    private void UpdateSelection()
    {

        currSelectionIdx = 0;

        Debug.Log("[COUNT2] " + tourSelections.Count);
        foreach (var item in tourSelections)
        {
            Debug.Log("[COUNT2] " + item.tourName);
        }

        AssetLoader.LoadSceneTextures(currentStartingScene).Then(() =>
        {
            UpdateListingDetails();
        })
        .Then(() => ShowCurrentPanorama(0.5f));
    }

    public void EnterTour() => App.Instance.EnterTour(currentSelection.tourConfig);

    public IPromise Hide()
    {
        var promises = linkContainer.Cast<Transform>().Select(link => link.GetComponent<LinkDisc>().Disable());
        return Promise.All(promises).Then(() => menuGameObject.SetActive(false));
    }

    public void HideImmediately() => menuGameObject.SetActive(false);
}