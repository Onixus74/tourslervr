﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class Scene
{
    public string code { get; set; }

    // the 3D coordinates of the scene (x and y on the floor, and z going up perpendicular from the floor)
    public float x { get; set; }
    public float y { get; set; }
    public float z { get; set; }
    public float yaw { get; set; }
    public string floor { get; set; }

    public Vector3 position => new Vector3(x, z, y);

    //public List<Link> links { get; set; }

    public List<string> linkedScenes { get; set; }

    // these fields are populated when processing the deserialized config
    public Cubemap cubemap { get; set; }

    public bool texturesQueuedForDownload { get; set; }
    public IEnumerable<Scene> scenes { get; set; }

    private int numTexturesLoaded = 0;
    public void SetTexture(Texture2D texture, CubemapFace face)
    {
        if (IsDoneLoadingTextures()) return;

        numTexturesLoaded++;
		var supportsGraphicsCopy = false; // galaxy s6 :-(
        if (supportsGraphicsCopy)
            Graphics.CopyTexture(texture, 0, 0, cubemap, (int) face, 0);
        else
        {
            cubemap.SetPixels(texture.GetPixels(), face);
            if (IsDoneLoadingTextures()) cubemap.Apply(false, true);
        }
    }

    public void ClearTextures()
    {
        texturesQueuedForDownload = false;
        numTexturesLoaded = 0;
        Object.Destroy(cubemap);
        Resources.UnloadUnusedAssets();
        cubemap = null;
    }

    public bool IsDoneLoadingTextures() => numTexturesLoaded == 6;

    public override bool Equals(object obj) => obj == null ? false : (obj as Scene).code == code;

    public override int GetHashCode() => code.GetHashCode();
}