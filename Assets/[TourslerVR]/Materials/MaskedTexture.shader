﻿Shader "MaskedTexture"
{
	Properties
	{
		_MainTex("Base (RGB)", 2D) = "grey" {}
		_Mask("Culling Mask", 2D) = "white" {}
	}
		SubShader
	{
		Tags{ "Queue" = "Transparent" }
		Lighting Off
		ZWrite Off
		Blend OneMinusSrcAlpha SrcAlpha
		Pass
	{
		SetTexture[_Mask]{ combine texture }
		SetTexture[_MainTex]{ combine texture, previous }
	}
	}
}

// Shaderlab syntax
// modified http://wiki.unity3d.com/index.php/TextureMask to invert black/white